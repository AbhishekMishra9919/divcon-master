API_KEY="nxrEeAiWFebKVjnIJccu1TeJZGVbsKIPHyW7U4TOaKRgeB8trXhAgdBNSqfk91f6"
commit_message=$(git log -1 --pretty=%B)
echo $commit_message

generate_post_data()
{
  cat <<EOF
{
  "protocol": 6706,
  "layout": 1,
  "periodicity": 0,
  "logs": true,
  "patching": false,
  "message_length": 8,
  "name": "gitlab",
  "description": "$commit_message",
  "duration": 120, 
  "file":"'$(cat basic.ihex | base64 -w0)'"
}
EOF
}

curl -X POST "https://iti-testbed.tugraz.at/api/queue/create_job?key=$API_KEY" \
-H "Content-Type: application/json" \
-d "$(generate_post_data)"
