import os
import sys
import subprocess
import time

script, common = sys.argv
max_iter = 300
n=12

os.system("sudo make TARGET=sky packetsync-test.upload")

for i in range(n):
	command = "sudo make TARGET=sky login MOTES=/dev/ttyUSB{} > results/{}-{}.txt".format(i,common,i+1)
	subprocess.Popen(command, shell=True)

time.sleep(max_iter*2)

os.system("sudo ps -ef | grep \"sky login\" | grep -v grep | awk '{print $2}' | sudo xargs kill")
