/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \defgroup glossy-test Simple mintlication for testing Glossy
 * @{
 */

/**
 * \file
 *         A simple example of an mintlication that uses Glossy, header file.
 *
 *         The mintlication schedules Glossy periodically.
 *         The period is determined by \link GLOSSY_PERIOD \endlink.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#ifndef GLOSSY_MINT_TEST_H_
#define GLOSSY_MINT_TEST_H_

#include "glossy.h"
#include "app.h"
#include "node-id.h"

/*------------------------------------------------------------------------------------*/
/**
 * \defgroup glossy-test-settings Application settings
 * @{
 */


/**
 * \brief NodeId of the initiator.
 *        Default value: 1
 */


// #define NUMBER_OF_NODES 3
// static uint8_t initiator_ids[NUMBER_OF_NODES] = {1,2,3};
// static uint8_t initiator_ids[27] = {1,2,3,4,6,7,8,10,11,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,31,32,33};
#define INITIATOR_NODE_ID 		1
#define MINT_INITIATOR_NODE_ID  1//(initiator_ids[packetsync_iter])

/**
 * \brief Application-specific header.
 *        Default value: 0x0
 */
#define APPLICATION_HEADER      0

/**
 * \brief Maximum number of transmissions N.
 *        Default value: 5.
 */
#define N_TX                    10
#define MINT_N_TX               5
#define CHAIN_LENGTH            103

/* ------------------------------------------------------------------------ */
/* This area has initiator information, neighbor information
for Round Robin */

#if EXP_LOCATION == LOCATION_LOCAL_CUSTOM
#define NUM_NODES 101
// static uint8_t initiator_ids[NUM_NODES] = { 36 };
// static uint8_t initiator_ids[NUM_NODES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
static uint8_t initiator_ids[NUM_NODES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101};
// static uint8_t initiator_ids[NUM_NODES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95};
// static uint8_t initiator_ids[NUM_NODES] = { 51,1,9,10,12,13,56,57,60,5,15,22,23,52,54,58,61,16,17,18,19,24,27,28,29,30,35,42,48,40,44,45,46,47,49,53,55 };
// static uint8_t initiator_ids[NUM_NODES] = { 1,2,3,4,5,6,7,8,9,10 };

// static uint8_t initiator_ids[NUM_NODES] = { 1,5,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,29,30,40,41,42,44,45,46,47,48,49,51,52,54,55,56,57,58,60,61 };
// static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,6,8,10,13,15,16,17,18,19,20,22,23,24,25,26,27,28,31,32,33};
// static uint8_t source_ids[NUM_NODES][NUM_NODES] = { {4,5,6,0},
// 													{3,5,6,0},
// 													{3,4,6,0},
// 													{3,4,5,0},
// 												};

/* --------------------------------------------------- */

#elif EXP_LOCATION == LOCATION_LOCAL

#define NUM_NODES 9
static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,6,7,8,11,16};
// static uint8_t source_ids[NUM_NODES][NUM_NODES] = { {2,3,0},
// 													{1,3,0},
// 													{1,2,0},
// 												};
/* --------------------------------------------------- */

#elif EXP_LOCATION == LOCATION_LOCAL_NEW

#define NUM_NODES 7
static uint8_t initiator_ids[NUM_NODES] = { 1,2,3,4,5,6,7 };
// static uint8_t source_ids[NUM_NODES][NUM_NODES] = { {2,3,4,5,6,7,0},
// 													{1,3,4,5,6,7,0},
// 													{1,2,4,5,6,7,0},
// 													{1,2,3,5,6,7,0},
// 													{1,2,3,4,6,7,0},
// 													{1,2,3,4,5,7,0},
// 													{1,2,3,4,5,6,0},
// 												};
/* --------------------------------------------------- */

#elif EXP_LOCATION == LOCATION_GATEWAY1

#define NUM_NODES 4
static uint8_t initiator_ids[NUM_NODES] = { 1,2,3,4 };
static uint8_t source_ids[NUM_NODES][NUM_NODES] = { {2,3,4,0},
													{1,3,4,0},
													{1,2,4,0},
													{1,2,3,0},
												};
/* --------------------------------------------------- */

#elif EXP_LOCATION == LOCATION_GATEWAY

#define NUM_NODES 8
static uint8_t initiator_ids[NUM_NODES] = { 1,2,3,4,5,6,7,9 };
static uint8_t source_ids[NUM_NODES][NUM_NODES] = { {2,3,4,0,0,0,0,0},
													{1,3,4,5,6,0,0,0},
													{1,2,4,7,0,0,0,0},
													{1,2,3,0,0,0,0,0},
													{2,6,7,9,0,0,0,0},
													{2,4,5,7,9,0,0,0},
													{3,5,6,0,0,0,0,0},
													{5,6,9,0,0,0,0,0},
												};
/* --------------------------------------------------- */

#elif EXP_LOCATION == LOCATION_INDRIYA

#define NUM_NODES 36
static uint8_t initiator_ids[NUM_NODES] = { 51,1,9,10,12,13,56,57,60,5,15,22,23,52,54,58,61,16,17,18,19,24,27,28,29,30,35,42,48,40,44,45,46,47,49,53,55 };
// static uint8_t initiator_ids[NUM_NODES] = { 1,5,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61 };
// static uint8_t source_ids[NUM_NODES][NUM_NODES] = {
// 													{7,9,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{13,12,15,10,7,1,47,49,51,26,27,22,46,23,0},
// 													{10,12,13,54,1,9,51,23,26,58,56,22,46,47,49,15,57,27,5,48,0},
// 													{10,12,54,1,7,13,51,23,26,27,49,0},
// 													{51,1,7,9,12,13,58,15,54,56,23,26,5,46,47,27,49,22,61,60,57,0},
// 													{9,15,54,7,10,1,58,13,46,47,5,22,23,51,26,49,27,55,48,0},
// 													{9,10,51,1,7,5,12,15,54,56,60,22,23,46,47,0},
// 													{12,60,10,13,55,29,20,5,27,1,0},
// 													{23,52,45,44,41,57,17,26,24,58,60,61,19,47,48,49,51,54,55,56,1,13,15,5,22,46,27,7,12,9,10,18,20,0},
// 													{23,24,27,41,18,22,57,55,26,16,19,52,58,60,61,47,48,49,51,54,56,5,7,9,12,1,15,20,30,10,45,42,29,40,44,13,0},
// 													{24,22,23,17,44,48,26,19,47,40,42,20,1,5,0},
// 													{17,26,24,23,22,27,18,29,55,52,16,57,42,7,13,0},
// 													{55,15,24,5,22,30,27,29,23,0},
// 													{23,24,30,18,17,42,52,48,57,26,20,19,41,5,7,9,10,12,40,58,60,61,27,29,45,47,49,54,55,56,51,13,15,44,46,1,16,0},
// 													{57,16,22,52,55,18,24,17,27,26,19,41,56,61,0},
// 													{22,23,52,20,44,18,57,17,26,41,19,42,46,27,51,40,47,16,48,55,10,12,13,15,49,5,58,60,61,29,30,45,0},
// 													{22,23,57,17,18,24,52,44,16,19,55,42,60,5,15,27,48,51,54,56,40,47,9,10,12,20,0},
// 													{23,17,55,19,20,29,9,26,15,0},
// 													{27,15,19,55,20,26,0},
// 													{22,20,5,0},
// 													{41,42,18,47,48,46,44,5,7,9,24,10,54,55,56,57,58,60,61,15,16,17,19,20,22,26,27,51,1,12,45,49,52,13,23,29,30,0},
// 													{44,45,46,48,42,17,22,16,47,24,57,58,60,61,5,7,9,10,23,1,12,49,52,54,55,56,27,29,30,40,51,26,13,15,18,19,20,0},
// 													{22,44,46,48,41,45,24,47,26,51,57,58,60,61,27,5,7,9,10,1,12,49,52,54,55,56,18,40,29,30,17,23,13,15,16,19,20,0},
// 													{45,46,24,48,41,42,18,26,23,16,47,57,58,60,61,5,7,9,1,10,12,49,52,54,55,56,51,22,40,27,29,30,13,15,17,19,20,0},
// 													{44,41,46,48,16,42,47,57,58,60,61,5,7,9,1,10,12,49,51,52,54,55,56,40,27,29,30,15,22,23,24,13,26,17,18,19,20,0},
// 													{44,41,45,48,42,47,57,58,60,61,5,7,9,10,1,12,49,51,52,54,55,56,18,40,26,27,29,30,17,22,23,24,13,15,16,19,20,0},
// 													{22,26,41,42,44,48,18,46,24,57,61,45,1,5,7,9,10,12,49,52,54,55,56,58,60,27,29,30,40,51,23,15,16,17,19,20,13,0},
// 													{41,42,44,45,46,22,18,24,47,9,5,23,26,57,60,61,1,10,49,52,54,55,56,40,58,27,29,30,51,7,12,13,15,16,17,19,20,0},
// 													{55,58,12,0},
// 													{1,5,23,10,13,56,60,57,7,9,58,54,26,27,46,47,48,22,12,0},
// 													{16,23,24,57,22,26,19,17,0},
// 													{9,1,7,10,13,58,12,51,61,57,22,23,46,47,56,0},
// 													{49,58,7,10,0},
// 													{51,57,1,5,10,49,61,58,60,13,9,22,7,26,27,0},
// 													{23,56,60,52,24,58,61,22,51,26,16,54,17,19,10,0},
// 													{54,55,56,61,49,57,10,12,7,51,1,13,5,26,0},
// 													{51,57,15,56,13,5,9,22,26,23,0},
// 													{57,58,56,10,54,22,0},
// 												};

#elif EXP_LOCATION == LOCATION_FLOCK_LAB

#define NUM_NODES 9
static uint8_t initiator_ids[NUM_NODES] = {2,4,5,6,7,8,9,10,11};
// static uint8_t source_ids[NUM_NODES][NUM_NODES] = {
// 													{2,3,4,8,15,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{1,3,4,8,15,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{2,4,6,8,15,16,22,23,26,28,31,32,33,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{1,2,3,8,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,16,18,22,27,28,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{1,2,3,4,15,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{11,25,26,31,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{10,13,25,26,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{11,17,20,23,25,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{1,2,3,4,8,31,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,18,22,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{13,19,20,23,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{16,20,22,23,24,27,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{13,17,20,23,24,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{13,17,18,19,22,23,24,25,26,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,16,18,24,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,13,18,20,24,27,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{18,22,23,27,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{11,13,26,31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{10,11,20,23,25,31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{6,18,20,23,24,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,6,16,18,20,22,23,27,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,10,15,25,26,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{3,10,15,31,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 													{1,2,3,6,8,16,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
// 												};

#elif EXP_LOCATION == LOCATION_DCUBE
#define NUM_NODES 28
// static uint8_t initiator_ids[NUM_NODES] = {118, 100, 101, 102, 103, 104, 105, 106, 107, 108, 110, 112, 113, 114, 115, 116, 117, 119, 213, 109, 211, 212, 219, 226, 111, 200, 210, 220, 201, 221, 225, 202, 215, 209, 216, 222, 227, 208, 217, 224, 223, 203, 218, 204, 205, 214, 206, 207};
// static uint8_t initiator_ids[NUM_NODES] = {221, 201, 220, 222, 200, 109, 202, 213, 215, 100, 103, 106, 107, 108, 112, 113, 209, 216, 217, 224, 104, 223, 101, 102, 105, 110, 114, 116, 203, 208, 218, 227, 111, 115, 117, 118, 119, 210, 211, 212, 214, 219, 226, 204, 205, 206, 225, 207 };
// static uint8_t initiator_ids[NUM_NODES] = {224, 203, 209, 216, 217, 218, 223, 204, 205, 215, 208, 214, 202, 206, 201, 221, 222, 227, 207, 200, 220, 100, 101, 103, 104, 106, 107, 109, 112, 113, 213, 108, 110, 115, 116, 210, 102, 105, 111, 114, 117, 118, 119, 211, 212, 219, 225, 226};
// static uint8_t initiator_ids[NUM_NODES] = {224,203,209,217,218,223,216,215,204,205,208,222,201,202,206};
// static uint8_t initiator_ids[NUM_NODES] = {221, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 222, 223, 224, 225, 226, 227};
// static uint8_t initiator_ids[NUM_NODES] = {118,100,113,101,102,103,105,106,108,110,112,114,115,116,117,119,104,107,213,109,211,219,212,200,226,220,111,210};

// static uint8_t initiator_ids[NUM_NODES] = {224,215,202,201,200,106,100,101,103,112,107};
// static uint8_t initiator_ids[NUM_NODES] = {201,202,200,215,106,224,203,209,217,218,223,216,118,100,113,101,102,103,105,108,110,112,114,115,116,117,119,104,107,213};

// static uint8_t initiator_ids[NUM_NODES] = {224,215,202,201,200,106,118};
// static uint8_t initiator_ids[NUM_NODES] = {210,112,200,201,202,215,224,209,216,223,203,217,218};
// static uint8_t initiator_ids[NUM_NODES] = {215,202,201,200,100,101,103,107,109,112,220};
// static uint8_t initiator_ids[NUM_NODES] = {202,201,215,200,224,209,216,223,100,101,107,109,112,103,220,203,217,218};
// static uint8_t initiator_ids[NUM_NODES] = {201};
// {224,203,209,217,218,223,216,215}
static uint8_t initiator_ids[NUM_NODES] = {224,203,209,217,218,223,216,215,202,201,200,220,100,103,113,109,101,106,107,112,104,213,108,116,210,110,115,118};


#endif

/* End of this section */
/* ------------------------------------------------------------------------ */
/**
 * \brief Period with which a Glossy phase is scheduled.
 *        Default value: 250 ms.
 */
#define GLOSSY_PERIOD 		(RTIMER_SECOND*2-2)//(RTIMER_SECOND / 4)      // 250 ms

/**
 * \brief Duration of each Glossy phase.
 *        Default value: 20 ms.
 */
#define GLOSSY_DURATION 	(RTIMER_SECOND / 8)//50)     //  20 ms
#define MINT_DURATION		(RTIMER_SECOND)
#define GAP (RTIMER_SECOND/128)	// This is the gap between the glossy
								// duration end and the start of the mint
								// can be a small value - but some operations
								// are there after the rtimer is scheduled
								// - it should be enough to execute that
								// example - estimate clock skew.

/**
 * \brief Guard-time at receivers.
 *        Default value: 526 us.
 */
#if COOJA
#define GLOSSY_GUARD_TIME 		(RTIMER_SECOND / 1000)
#else
#define GLOSSY_GUARD_TIME       (RTIMER_SECOND / 1900)   // 526 us
#endif /* COOJA */

#define APP_GUARD_TIME			(RTIMER_SECOND / 1024)
/**
 * \brief Number of consecutive Glossy phases with successful computation of reference time required to exit from bootstrminting.
 *        Default value: 3.
 */
#define GLOSSY_BOOTSTRAP_PERIODS 3

/**
 * \brief Period during bootstrminting at receivers.
 *        It should not be an exact fraction of \link GLOSSY_PERIOD \endlink.
 *        Default value: 69.474 ms.
 */
#define GLOSSY_INIT_PERIOD      (GLOSSY_INIT_DURATION + RTIMER_SECOND / 100)                   //  69.474 ms

/**
 * \brief Duration during bootstrminting at receivers.
 *        Default value: 59.474 ms.
 */

#define GLOSSY_INIT_DURATION    (GLOSSY_DURATION - GLOSSY_GUARD_TIME + GLOSSY_INIT_GUARD_TIME) //  59.474 ms

/**
 * \brief Guard-time during bootstrminting at receivers.
 *        Default value: 50 ms.
 */
#define GLOSSY_INIT_GUARD_TIME  (RTIMER_SECOND / 20)                                           //  50 ms

/**
 * \brief Data structure used to represent flooding data.
 */
typedef struct {
	unsigned long mint_seq_no; /**< Sequence number, incremented by the initiator at each Glossy phase. */
} mint_data_struct;

typedef struct {
	unsigned long seq_no; /**< Sequence number, incremented by the initiator at each Glossy phase. */
} glossy_data_struct;

/** @} */

/**
 * \defgroup glossy-test-defines Application internal defines
 * @{
 */

/**
 * \brief Length of data structure.
 */
#define DATA_LEN                    sizeof(glossy_data_struct)
#define MINT_DATA_LEN               sizeof(mint_data_struct)

/**
 * \brief Check if the nodeId matches the one of the initiator.
 */
#define IS_INITIATOR()              (node_id == INITIATOR_NODE_ID)
#define IS_MINT_INITIATOR()         (node_id == MINT_INITIATOR_NODE_ID)

/**
 * \brief Check if Glossy is still bootstrminting.
 * \sa \link GLOSSY_BOOTSTRAP_PERIODS \endlink.
 */
#define GLOSSY_IS_BOOTSTRAPPING()   (skew_estimated < GLOSSY_BOOTSTRAP_PERIODS)

/**
 * \brief Check if Glossy is synchronized.
 *
 * The mintlication assumes that a node is synchronized if it updated the reference time
 * during the last Glossy phase.
 * \sa \link is_t_ref_l_updated \endlink
 */
#define GLOSSY_IS_SYNCED()          (is_t_ref_l_updated())

/**
 * \brief Get Glossy reference time.
 * \sa \link get_t_ref_l \endlink
 */
#define GLOSSY_REFERENCE_TIME       (get_t_ref_l())

/** @} */

/** @} */

#endif /* GLOSSY_TEST_H_ */
