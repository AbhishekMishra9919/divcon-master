#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

#undef COOJA
#define COOJA	1

#define LOCATION_LOCAL 1
#define LOCATION_GATEWAY1 2
#define LOCATION_GATEWAY 3
#define LOCATION_INDRIYA 4
#define LOCATION_FLOCK_LAB 5
#define LOCATION_LOCAL_NEW 6
#define LOCATION_LOCAL_CUSTOM 7

#define MAX_POWER_LEVEL 30	
#define MIN_POWER_LEVEL 0
#define EXP_LOCATION LOCATION_LOCAL_CUSTOM

#endif /* PROJECT_ERBIUM_CONF_H_ */
