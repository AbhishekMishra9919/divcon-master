/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 *
 * \mainpage
 *           These files document the source code of Glossy, a flooding architecture for wireless sensor networks,
 *           implemented in <a href="http://www.sics.se/contiki/">Contiki</a> based on Tmote Sky sensor nodes.
 *
 *           Glossy was published at ACM/IEEE IPSN '11 in the paper titled
 *           <a href="ftp://ftp.tik.ee.ethz.ch/pub/people/ferrarif/FZTS2011.pdf">
 *           Efficient network flooding and time synchronization with Glossy</a>,
 *           which also received the best paper award.
 *
 *           This documentation is divided into three main parts:
 *           \li \ref glossy-test "Simple mintlication for testing Glossy":
 *           Example of a simple mintlication that periodically floods a packet and prints related statistics.
 *           \li \ref glossy_interface "Glossy API":
 *           API provided by Glossy for an mintlication that wants to use it.
 *           \li \ref glossy_internal "Glossy internal functions":
 *           Functions used internally by Glossy during a flood.
 *
 *           A complete overview of the documentation structure is available <a href="modules.html">here</a>.
 *
 * \author
 *           <a href="http://www.tik.ee.ethz.ch/~ferrarif">Federico Ferrari</a> <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \defgroup glossy-test Simple mintlication for testing Glossy
 *
 *           This mintlication runs Glossy periodically to flood a packet from one node (initiator)
 *           to the other nodes (receivers) and prints flooding-related statistics.
 *
 *           The mintlication schedules Glossy periodically with a fixed period \link GLOSSY_PERIOD \endlink.
 *
 *           The duration of each Glossy phase is given by \link GLOSSY_DURATION \endlink.
 *
 *           During each Glossy phase, the maximum number of transmissions in Glossy (N)
 *           is set to \link N_TX \endlink.
 *
 *           The initiator of the floods is the node having nodeId \link INITIATOR_NODE_ID \endlink.
 *
 *           The packet to be flooded has the format specified by data structure \link glossy_data_struct \endlink.
 *
 *           Receivers synchronize by computing the reference time during each Glossy phase.
 *
 *           To synchronize fast, at startup receivers run Glossy with a significantly shorter period
 *           (\link GLOSSY_INIT_PERIOD \endlink) and longer duration (\link GLOSSY_INIT_DURATION \endlink).
 *
 *           Receivers exit the bootstrminting phase when they have computed the reference time for
 *           \link GLOSSY_BOOTSTRAP_PERIODS \endlink consecutive Glossy phases.
 *
 * @{
 */

/**
 * \file
 *         A simple example of an mintlication that uses Glossy, source file.
 *
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#include "app.h"
#include "app_oa.h"

#include "glossy-app-sync-test.h"
#include "project-conf.h"


char mint_scheduler(struct rtimer *t, void *ptr);
char mint_alt_scheduler(struct rtimer *t, void *ptr);
/**additions**/
/**
 * \defgroup glossy-test-variables mintlication variables
 * @{
 */

/**
 * \defgroup glossy-test-variables-sched-sync Scheduling and synchronization variables
 * @{
 */

static glossy_data_struct glossy_data;     /**< \brief Flooding data. */

static struct rtimer rt;                   /**< \brief Rtimer used to schedule Glossy. */
static struct pt pt;                       /**< \brief Protothread used to schedule Glossy. */

static rtimer_clock_t t_ref_l_old = 0;     /**< \brief Reference time computed from the Glossy
                                                phase before the last one. \sa get_t_ref_l */
static uint8_t skew_estimated = 0;         /**< \brief Not zero if the clock skew over a period of length
                                                \link GLOSSY_PERIOD \endlink has already been estimated. */
static uint8_t sync_missed = 0;            /**< \brief Current number of consecutive phases without
                                                synchronization (reference time not computed). */
static rtimer_clock_t t_start = 0;         /**< \brief Starting time (low-frequency clock)
                                                of the last Glossy phase. */
static int period_skew = 0;                /**< \brief Current estimation of clock skew over a period



/** @} */

// New variables

static struct pt mint_pt;
static rtimer_clock_t t_mint_start = 0;

static struct pt mint_alt_pt;
// newer variables.

/**
 * \defgroup glossy-test-variables-stats Statistics variables
 * @{
 */

static unsigned long glossy_packets_received = 0; /**< \brief Current number of received packets. */
static unsigned long glossy_packets_missed = 0;   /**< \brief Current number of missed packets. */
static unsigned long glossy_latency = 0;          /**< \brief Latency of last Glossy phase, in us. */
static unsigned long glossy_sum_latency = 0;      /**< \brief Current sum of latencies, in ticks of low-frequency
                                                clock (used to compute average). */
static unsigned long mint_packets_received = 0; /**< \brief Current number of received packets. */
static unsigned long mint_packets_missed = 0;   /**< \brief Current number of missed packets. */
static unsigned long mint_latency = 0;          /**< \brief Latency of last Glossy phase, in us. */
static unsigned long mint_sum_latency = 0;      /**< \brief Current sum of latencies, in ticks of low-frequency **/

static unsigned long glossy_transmit,glossy_listen,app_transmit,app_listen,total_transmit,total_listen;


/* Round Robin Related Functions */
/* ------------------------------------------------------------------------ */
uint8_t initiator_flag = 0;
uint8_t packetsync_iter = 0;
uint8_t pos;

/*group control related functions.*/
static uint8_t group_number = 10;

// variables for data.
static uint8_t dummy_data, ret_data;
static int retcon_data;

void set_initiator_flag(){
/* We use sequence no of glossy_data to set the next initiator here
Since, this glossy data is dessiminated by glossy initiator to rest
of the network, it is synchronised across all the nodes */

	packetsync_iter = (glossy_data.seq_no - 1)%NUM_NODES;
	// packetsync_iter = 5;

/* The next initiator is initiator_ids[packetsync_iter]
We now set the initiator flag if this node is the initiator node */

	if(initiator_ids[packetsync_iter] == node_id) initiator_flag=1;
	else initiator_flag=0;
}

//void set_pos(){
//	uint8_t i;
//	pos = 0;
//
//	for(i=0;i<NUM_NODES;i++)
//		if(initiator_ids[i] == node_id)
//			pos = i+1;
//}
void set_pos()
{
	 uint8_t i;
	 pos = 0;
	 for(i = 0;i < MAX_NUMBER_NODES_PER_GROUP;i++)
	 	if(grps[GROUP_NUMBER][i] == node_id)
			pos = i + 1;
}

// channel change operations.


// Two-group channel condition - in this case, apply this function twice.
uint8_t grp_channel_operation(void)
{
  if(group_number == 0)
		return 20;
	else
		return 14;
}


/*
// Three group channel operation.
uint8_t grp_channel_operation_first(void)
{
	if(group_number == 0 && grps[0][1] != node_id||group_number == 1 && grps[1][1] == node_id)
		return 20;
	else if(group_number == 1 && grps[0][1] != node_id||group_number == 2 && grps[2][1] == node_id)
			return 18;
	else
		 return 16;
}
uint8_t grp_channel_operation_second(void)
{
	if(group_number == 0 && grps[0][1] != node_id||group_number == 2 && grps[2][1] == node_id)
			return 20;
	else if(group_number == 1 && grps[0][1] != node_id||group_number == 0 && grps[0][1] == node_id)
			return 18;
	else
	 		return 16;
}
*/

/*
	// Four-group channel operation.
	uint8_t grp_channel_operation_first(void)
	{
	if(group_number == 0 && grps[0][1] != node_id||group_number == 1 && grps[1][1] == node_id)
		return 20;
	else if(group_number == 1 && grps[1][1] != node_id||group_number == 2 && grps[2][1] == node_id)
			return 18;
	else if(group_number == 2 && grps[2][1] != node_id||group_number == 3 && grps[3][1] == node_id)
		 return 16;
	else
		return 14;

	}
	uint8_t grp_channel_operation_second(void)
	{
	if(group_number == 0 && grps[0][1] != node_id||group_number == 2 && grps[2][1] == node_id)
		return 20;
	else if(group_number == 1 && grps[1][1] != node_id||group_number == 3 && grps[3][1] == node_id)
			return 18;
	else if(group_number == 2 && grps[2][1] != node_id||group_number == 0 && grps[0][1] == node_id)
		 return 16;
	else
		return 14;
	}
	uint8_t grp_channel_operation_third(void)
	{
	if(group_number == 0 && grps[0][1] != node_id||group_number == 3 && grps[3][1] == node_id)
		return 20;
	else if(group_number == 1 && grps[1][1] != node_id||group_number == 0 && grps[0][1] == node_id)
			return 18;
	else if(group_number == 2 && grps[2][1] != node_id||group_number == 1 && grps[1][1] == node_id)
		 return 16;
	else
		return 14;
	}
*/

/* ------------------------------------------------------------------------ */

static void print_glossy_stats(){

	if (get_rx_cnt()) {	// Packet received at least once.
		// Increment number of successfully received packets.
		glossy_packets_received++;
		// Compute latency during last Glossy phase.
		rtimer_clock_t lat = get_t_first_rx_l() - get_t_ref_l();
		// Add last latency to sum of latencies.
		glossy_sum_latency += lat;
		// Convert latency to microseconds.
		glossy_latency = (unsigned long)(lat) * 1e6 / RTIMER_SECOND;
		// Print information about last packet and related latency.
		//				printf("Glossy received %u time%s: seq_no %lu, latency %lu.%03lu ms\n",
		//						get_rx_cnt(), (get_rx_cnt() > 1) ? "s" : "", glossy_data.seq_no,
		//								latency / 1000, latency % 1000);
		//printf("%5lu %4u %4lu.%03lu ", glossy_data.seq_no, get_rx_cnt(), glossy_latency / 1000, glossy_latency % 1000);
		printf("%4lu %4d | ", glossy_data.seq_no,get_rx_cnt());


	} else {	// Packet not received.
		// Increment number of missed packets.
		glossy_packets_missed++;
		// Print failed reception.
		//printf("Glossy NOT received\n");
		printf("%4lu %4d | ",glossy_data.seq_no,get_rx_cnt());


	}
#if GLOSSY_DEBUG
	//			printf("skew %ld ppm\n", (long)(period_skew * 1e6) / GLOSSY_PERIOD);
	printf("high_T_irq %u, rx_timeout %u, bad_length %u, bad_header %u, bad_crc %u\n",
			high_T_irq, rx_timeout, bad_length, bad_header, bad_crc);
#endif /* GLOSSY_DEBUG */
	// Compute current average reliability.
	unsigned long avg_rel = glossy_packets_received * 1e5 / (glossy_packets_received + glossy_packets_missed);
	// Print information about average reliability.
	//			printf("average reliability %3lu.%03lu %% ",
	//					avg_rel / 1000, avg_rel % 1000);
	//			printf("(missed %lu out of %lu packets)\n",

	//printf(" %3lu.%03lu %% ", avg_rel / 1000, avg_rel % 1000);
	// printf("%7lu %7lu", glossy_packets_received, glossy_packets_missed);

	//#if ENERGEST_CONF_ON
	//	// Compute average radio-on time, in microseconds.
	//	unsigned long avg_radio_on = (unsigned long)GLOSSY_PERIOD * 1e6 / RTIMER_SECOND *
	//			(energest_type_time(ENERGEST_TYPE_LISTEN) + energest_type_time(ENERGEST_TYPE_TRANSMIT)) /
	//			(energest_type_time(ENERGEST_TYPE_CPU) + energest_type_time(ENERGEST_TYPE_LPM));
	//	// Print information about average radio-on time.
	////			printf("average radio-on time %lu.%03lu ms\n",
	////					avg_radio_on / 1000, avg_radio_on % 1000);
	//	printf("%4lu.%03lu ", avg_radio_on / 1000, avg_radio_on % 1000);
	//
	//#endif /* ENERGEST_CONF_ON */
	// Compute average latency, in microseconds.
	unsigned long glossy_avg_latency = glossy_sum_latency * 1e6 / (RTIMER_SECOND * glossy_packets_received);
	// Print information about average latency.
	//			printf("average latency %lu.%03lu ms\n",
	//					avg_latency / 1000, avg_latency % 1000);
	//printf("%4lu.%03lu ", glossy_avg_latency / 1000, glossy_avg_latency % 1000);

	//print_glossy_states();

}
static void print_mint_stats(){

	//latency calculation
	// rtimer_clock_t lat = get_t_end() - get_t_start();
	// mint_sum_latency += lat;
	// mint_latency = (unsigned long)(lat) * 1e6 / RTIMER_SECOND;

	//latency printing
	// printf("%4lu.%03lu | ", mint_latency / 1000, mint_latency % 1000);

	//reliability calculation
	unsigned long val = get_rel_cnt();
	unsigned long avg_rel;

	mint_packets_received = CHAIN_LENGTH;
	mint_packets_missed = val - CHAIN_LENGTH;

	// if (val>0) {	// Packet received at least once.
	// 	// Increment number of successfully received packets.
	// 	mint_packets_received++;
	// 	printf("%4u %4u | ", get_app_tx_cnt(), get_app_rx_cnt());

	// } else {	// Packet not received.

	// 	// Increment number of missed packets.
	// 	mint_packets_missed++;
	// 	printf("%4u %4u | ", get_app_tx_cnt(), get_app_rx_cnt());
	// }

#if mint_DEBUG
	//			printf("skew %ld ppm\n", (long)(period_skew * 1e6) / GLOSSY_PERIOD);
	printf("high_T_irq %u, rx_timeout %u, bad_length %u, bad_header %u, bad_crc %u\n",
			high_T_irq, rx_timeout, bad_length, bad_header, bad_crc);
#endif /* GLOSSY_DEBUG */

	// Compute current average reliability.
	avg_rel = mint_packets_received * 1e5 / (mint_packets_received + mint_packets_missed);
	//	printf(" %3lu.%03lu %% ", avg_rel / 1000, avg_rel % 1000);
	// printf("%7lu %7lu ", mint_packets_received, mint_packets_missed);

	total_transmit = energest_type_time(ENERGEST_TYPE_TRANSMIT)*1e6/RTIMER_SECOND;
	total_listen = energest_type_time(ENERGEST_TYPE_LISTEN)*1e6/RTIMER_SECOND;

	app_transmit = total_transmit - glossy_transmit;
	app_listen = total_listen - glossy_listen;

	//#if ENERGEST_CONF_ON
	//	// Compute average radio-on time, in microseconds.
	//	// Has to be calculated properly -
	//	unsigned long avg_radio_on = (unsigned long)GLOSSY_PERIOD * 1e6 / RTIMER_SECOND *
	//			(energest_type_time(ENERGEST_TYPE_LISTEN) + energest_type_time(ENERGEST_TYPE_TRANSMIT)) /
	//			(energest_type_time(ENERGEST_TYPE_CPU) + energest_type_time(ENERGEST_TYPE_LPM));
	//	printf("%4lu.%03lu ", avg_radio_on / 1000, avg_radio_on % 1000);
	//
	//#endif /* ENERGEST_CONF_ON */
	//	// Compute average latency, in microseconds.
	//	unsigned long mint_avg_latency = mint_sum_latency * 1e6 / (RTIMER_SECOND * mint_packets_received);
	//	printf("%4lu.%03lu ", mint_avg_latency / 1000, mint_avg_latency % 1000);
	// if (get_app_rx_cnt()) {
	// 	printf("%7d",get_app_relay_cnt()+1);
	// }
	// else
	// 	printf("%7d",0);

	print_app_states();
	// printf(" | ");
	// print_power_array();

}


PROCESS(print_stats_process, "print stats");
PROCESS_THREAD(print_stats_process, ev, data)
{
	PROCESS_BEGIN();

	while(1) {
		PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
		// Print statistics only if Glossy is not still bootstrminting.

		if (!GLOSSY_IS_BOOTSTRAPPING()) {
			printf(" | ");

			printf("%2u %2u", node_id, MINT_INITIATOR_NODE_ID);

			printf(" |");

			print_glossy_stats();

			// printf(" |");

			print_mint_stats();

			printf("\n");

		}
	}

	PROCESS_END();
}

/** @} */

/**
 * \defgroup glossy-test-skew Clock skew estimation
 * @{
 */

static inline void estimate_period_skew(void) {
	// Estimate clock skew over a period only if the reference time has been updated.
	if (GLOSSY_IS_SYNCED()) {
		// Estimate clock skew based on previous reference time and the Glossy period.
		period_skew = get_t_ref_l() - (t_ref_l_old + (rtimer_clock_t)GLOSSY_PERIOD);
		// Update old reference time with the newer one.
		t_ref_l_old = get_t_ref_l();
		// If Glossy is still bootstrminting, count the number of consecutive updates of the reference time.
		if (GLOSSY_IS_BOOTSTRAPPING()) {
			// Increment number of consecutive updates of the reference time.
			skew_estimated++;
			// Check if Glossy has exited from bootstrminting.
			if (!GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy has exited from bootstrminting.
				leds_off(LEDS_RED);
				// Initialize Energest values.
				energest_init();
#if GLOSSY_DEBUG
				high_T_irq = 0;
				bad_crc = 0;
				bad_length = 0;
				bad_header = 0;
#endif /* GLOSSY_DEBUG */

			}
		}
	}
}

/** @} */

/**
 * \defgroup glossy-test-scheduler Periodic scheduling
 * @{
 */

char glossy_scheduler(struct rtimer *t, void *ptr) {
	PT_BEGIN(&pt);

	if (IS_INITIATOR()) {	// Glossy initiator.
		while (1) {
			// Increment sequence number.
			cc2420_set_channel(CHANNEL_TO_SYNC);
			glossy_data.seq_no++;
			// Glossy phase.
			leds_on(LEDS_GREEN);
			rtimer_clock_t t_stop = RTIMER_TIME(t) + GLOSSY_DURATION;
			// Start Glossy.
			glossy_start((uint8_t *)&glossy_data, DATA_LEN, GLOSSY_INITIATOR, GLOSSY_SYNC, N_TX,
					APPLICATION_HEADER, t_stop, (rtimer_callback_t)glossy_scheduler, t, ptr);
			// Store time at which Glossy has started.
			t_start = RTIMER_TIME(t);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&pt);

			// Off phase.
			leds_off(LEDS_GREEN);
			// Stop Glossy.
			glossy_stop();
			if (!GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy has already successfully bootstrminted.
				if (!GLOSSY_IS_SYNCED()) {
					// The reference time was not updated: increment reference time by GLOSSY_PERIOD.
					set_t_ref_l(GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD);
					set_t_ref_l_updated(1);

				}


				/*---------------------------------------------------------------------------------------*/
				// Here you are going into inner glossy
				// But when you should schedule your inner glossy (mint) should depend on you are the
				// initiator of the inner glossy or receiver. So, discriminate these two cases.
				// Since - Receiver should start little earlier than the initiator


				glossy_transmit = energest_type_time(ENERGEST_TYPE_TRANSMIT)*1e6/RTIMER_SECOND;
				glossy_listen = energest_type_time(ENERGEST_TYPE_LISTEN)*1e6/RTIMER_SECOND;

				if(glossy_data.seq_no >= 21){
					set_initiator_flag();
					set_pos();
				}

				if (IS_MINT_INITIATOR()) {	// mint initiator.

					rtimer_set(t, t_start + GLOSSY_DURATION + GAP, 1,
							(rtimer_callback_t)mint_scheduler, ptr);
				} else {
					//Glossy has already successfully bootstrminted:
					rtimer_set(t, t_start + (GLOSSY_DURATION+GAP) - APP_GUARD_TIME, 1,
							(rtimer_callback_t)mint_scheduler, ptr);
				}
				controller = CNTRL_APP;
				/*---------------------------------------------------------------------------------------*/


			} else {

				rtimer_set(t, t_start + GLOSSY_PERIOD, 1, (rtimer_callback_t)glossy_scheduler, ptr);
				// if still bootstrminting
			}

			// Schedule begin of next Glossy phase based on GLOSSY_PERIOD.
			// Estimate the clock skew over the last period.
			estimate_period_skew();

			// Yield the protothread.
			PT_YIELD(&pt);
		}
	} else {	// Glossy receiver.
		while (1) {
			// Glossy phase.
			cc2420_set_channel(CHANNEL_TO_SYNC);
			leds_on(LEDS_GREEN);
			rtimer_clock_t t_stop;
			if (GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy is still bootstrminting:
				// Schedule end of Glossy phase based on GLOSSY_INIT_DURATION.
				t_stop = RTIMER_TIME(t) + GLOSSY_INIT_DURATION;
			} else {
				// Glossy has already successfully bootstrminted:
				// Schedule end of Glossy phase based on GLOSSY_DURATION.
				t_stop = RTIMER_TIME(t) + GLOSSY_DURATION;
			}
			// Start Glossy.
			glossy_start((uint8_t *)&glossy_data, DATA_LEN, GLOSSY_RECEIVER, GLOSSY_SYNC, N_TX,
					APPLICATION_HEADER, t_stop, (rtimer_callback_t)glossy_scheduler, t, ptr);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&pt);

			// Off phase.
			leds_off(LEDS_GREEN);
			// Stop Glossy.
			glossy_stop();
			if (GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy is still bootstrminting.
				if (!GLOSSY_IS_SYNCED()) {
					// The reference time was not updated: reset skew_estimated to zero.
					skew_estimated = 0;
				}
			} else {
				// Glossy has already successfully bootstrminted.
				if (!GLOSSY_IS_SYNCED()) {
					// The reference time was not updated:
					// increment reference time by GLOSSY_PERIOD + period_skew.
					set_t_ref_l(GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD + period_skew);
					set_t_ref_l_updated(1);
					// Increment sync_missed.
					sync_missed++;
				} else {
					// The reference time was not updated: reset sync_missed to zero.
					sync_missed = 0;
				}
			}
			// Estimate the clock skew over the last period.
			estimate_period_skew();
			if (GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy is still bootstrminting.
				if (skew_estimated == 0) {
					// The reference time was not updated:
					// Schedule begin of next Glossy phase based on last begin and GLOSSY_INIT_PERIOD.
					rtimer_set(t, RTIMER_TIME(t) + GLOSSY_INIT_PERIOD, 1,
							(rtimer_callback_t)glossy_scheduler, ptr);
				} else {
					// The reference time was updated:
					// Schedule begin of next Glossy phase based on reference time and GLOSSY_INIT_PERIOD.
					rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD - GLOSSY_INIT_GUARD_TIME, 1,
							(rtimer_callback_t)glossy_scheduler, ptr);
				}
			} else {


				/*---------------------------------------------------------------------------------------*/
				// Here you are going into inner glossy
				// But when you should schedule your inner glossy (mint) should depend on you are the
				// initiator of the inner glossy or receiver. So, discriminate these two cases.
				// Since - Receiver should start little earlier than the initiator

				glossy_transmit = energest_type_time(ENERGEST_TYPE_TRANSMIT)*1e6/RTIMER_SECOND;
				glossy_listen = energest_type_time(ENERGEST_TYPE_LISTEN)*1e6/RTIMER_SECOND;

				if(glossy_data.seq_no >= 21){
					set_initiator_flag();
					set_pos();
				}

				if (IS_MINT_INITIATOR()) {	// mint initiator.
					rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_DURATION + GAP, 1,
							(rtimer_callback_t)mint_scheduler, ptr);

				} else {

					//Glossy has already successfully bootstrminted:
					rtimer_set(t, GLOSSY_REFERENCE_TIME + (GLOSSY_DURATION+GAP) - APP_GUARD_TIME, 1,
							(rtimer_callback_t)mint_scheduler, ptr);

				}
				controller = CNTRL_APP;
				/*---------------------------------------------------------------------------------------*/

			}
			// Yield the protothread.
			PT_YIELD(&pt);

		}
	}

	PT_END(&pt);
}

// N-group situation.
char mint_scheduler(struct rtimer *t, void *ptr) {

	PT_BEGIN(&mint_pt);

	while (1) {
		if (IS_MINT_INITIATOR()) {	// mint initiator.
			// Increment sequence number.
			//glossy_data.seq_no++;
			leds_on(LEDS_BLUE);
			rtimer_clock_t t_mint_stop = RTIMER_TIME(t) + MINT_DURATION;

			// Start Glossy.
			app_start(APP_INITIATOR, pos, NUM_NODES, GROUP_NUMBER, MINT_N_TX, CHAIN_LENGTH,
				APPLICATION_HEADER, t_mint_stop, dummy_data, (rtimer_callback_t)mint_scheduler, t, ptr);

			// Store time at which Glossy has started.
			t_mint_start = RTIMER_TIME(t);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&mint_pt);
			// Off phase.
			leds_off(LEDS_BLUE);
			// Stop Glossy.
			app_stop();
			// Here you are going back to outer glossy
			// But when you should schedule your outer glossy (wrminter) that should depend on whether
			// you are the initiator of the outer glossy or receiver. So, discriminate these two cases.
			// Since - Receiver should start little earlier than the initiator
			// Template to segway.
			retcon_data = get_app_alt_data();
			ret_data = compute();
			if(IS_MINT_ALT_INITIATOR())
			{
				if(IS_INITIATOR())
					rtimer_set(t, t_start+ GLOSSY_DURATION + GAP + APP_DURATION + GAP, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
				else
					rtimer_set(t, GLOSSY_REFERENCE_TIME+ GLOSSY_DURATION + GAP + APP_DURATION + GAP, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
			}
			else{
			if(IS_INITIATOR())
				rtimer_set(t, t_start+ GLOSSY_DURATION + GAP + APP_DURATION + GAP - GLOSSY_GUARD_TIME, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
			else
				rtimer_set(t, GLOSSY_REFERENCE_TIME+ GLOSSY_DURATION + GAP + APP_DURATION + GAP - GLOSSY_GUARD_TIME, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
			}
			controller = CNTRL_MINT_ALT;
			process_poll(&print_stats_process);
			// Yield the protothread.
			PT_YIELD(&mint_pt);
		} else {	// mint receiver.
			// Glossy phase.
			leds_on(LEDS_BLUE);
			rtimer_clock_t t_mint_stop = RTIMER_TIME(t) + MINT_DURATION;


			app_start(APP_RECEIVER, pos, NUM_NODES, MINT_N_TX, CHAIN_LENGTH,
				APPLICATION_HEADER, t_mint_stop, dummy_data, (rtimer_callback_t)mint_scheduler, t, ptr);
			// Yield the protothread. It will be resumed when mint terminates.
			PT_YIELD(&mint_pt);

			// Off phase.
			leds_off(LEDS_BLUE);
			// Stop mint.
			app_stop();
			// Here you are going back to outer glossy
			// But when you should schedule your outer glossy (wrminter) that should depend on whether
			// you are the initiator of the outer glossy or receiver. So, discriminate these two cases.
			// Since - Receiver should start little earlier than the initiator
			// Template to segway.
			retcon_data = get_app_alt_data();
			ret_data = compute();
			if(IS_MINT_ALT_INITIATOR())
			{
				if(IS_INITIATOR())
					rtimer_set(t, t_start+ GLOSSY_DURATION + GAP + APP_DURATION + GAP, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
				else
					rtimer_set(t, GLOSSY_REFERENCE_TIME+ GLOSSY_DURATION + GAP + APP_DURATION + GAP, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
			}
			else{
			if(IS_INITIATOR())
				rtimer_set(t, t_start+ GLOSSY_DURATION + GAP + APP_DURATION + GAP - GLOSSY_GUARD_TIME, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
			else
				rtimer_set(t, GLOSSY_REFERENCE_TIME+ GLOSSY_DURATION + GAP + APP_DURATION + GAP - GLOSSY_GUARD_TIME, 1, (rtimer_callback_t)mint_alt_scheduler, ptr);
			}
			controller = CNTRL_APP_ALT;
			process_poll(&print_stats_process);
			// Yield the protothread.
			PT_YIELD(&mint_pt);
		}
		fast_set_power(MAX_POWER_LEVEL);
	}
	PT_END(&mint_pt);
}

char mint_alt_scheduler(struct rtimer *t, void *ptr) {

	PT_BEGIN(&mint_alt_pt);
	while (1) {
		if (IS_MINT_ALT_INITIATOR()) {	// mint initiator.
			// Increment sequence number.
			//glossy_data.seq_no++;
			leds_on(LEDS_BLUE);
			rtimer_clock_t t_mint_alt_stop = RTIMER_TIME(t) + MINT_ALT_DURATION;
			// Start Glossy.
			app_alt_start(APP_ALT_INITIATOR, group_number, NUM_GROUPS, 0, MINT_ALT_N_TX, CHAIN_ALT_LENGTH,
				APPLICATION_HEADER, t_mint_alt_stop, ret_data, (rtimer_callback_t)mint_alt_scheduler, t, ptr);
			// Store time at which Glossy has started.
			t_mint_alt_start = RTIMER_TIME(t);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&mint_alt_pt);
			// Off phase.
			leds_off(LEDS_BLUE);
			// Stop Glossy.
			app_alt_stop();
			// Here you are going back to outer glossy
			// But when you should schedule your outer glossy (wrminter) that should depend on whether
			// you are the initiator of the outer glossy or receiver. So, discriminate these two cases.
			// Since - Receiver should start little earlier than the initiator
			if(IS_INITIATOR()){
				rtimer_set(t, t_start + GLOSSY_PERIOD, 1, (rtimer_callback_t)glossy_scheduler, ptr);
			}else {
				rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD +
						period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
						(rtimer_callback_t)glossy_scheduler, ptr);
			}
			controller = CNTRL_GLOSSY;
			process_poll(&print_stats_process);
			// Yield the protothread.
			PT_YIELD(&mint_pt);
		} else {	// mint receiver.
			// Glossy phase.
			leds_on(LEDS_BLUE);
			rtimer_clock_t t_mint_alt_stop = RTIMER_TIME(t) + MINT_ALT_DURATION;
			app_alt_start(APP_RECEIVER, pos, NUM_GROUPS, MINT_ALT_N_TX, CHAIN_LENGTH,
				APPLICATION_HEADER, t_mint_alt_stop, ret_data, (rtimer_callback_t)mint_alt_scheduler, t, ptr);
			// Yield the protothread. It will be resumed when mint terminates.
			PT_YIELD(&mint_alt_pt);
			// Off phase.
			leds_off(LEDS_BLUE);
			// Stop mint.
			app_alt_stop();
			// Here you are going back to outer glossy
			// But when you should schedule your outer glossy (wrminter) that should depend on whether
			// you are the initiator of the outer glossy or receiver. So, discriminate these two cases.
			// Since - Receiver should start little earlier than the initiator
			if(IS_INITIATOR()){
				rtimer_set(t, t_start + GLOSSY_PERIOD, 1, (rtimer_callback_t)glossy_scheduler, ptr);
			}else {
				rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD +
						period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
						(rtimer_callback_t)glossy_scheduler, ptr);
			}
			controller = CNTRL_GLOSSY;
			process_poll(&print_stats_process);
			// Yield the protothread.
			PT_YIELD(&mint_alt_pt);
		}
		fast_set_power(MAX_POWER_LEVEL);
	}
	PT_END(&mint_alt_pt);
}

PROCESS(glossy_test, "Glossy test");
AUTOSTART_PROCESSES(&glossy_test);
PROCESS_THREAD(glossy_test, ev, data)
{
	PROCESS_BEGIN();

	leds_on(LEDS_RED);
	//	int i;
	//	for(i=0;;i++)
	//	{
	//	printf("%d\n",node_id);
	//	}
	// Initialize Glossy data.
	glossy_data.seq_no = 0;
	packetsync_iter = 0;
	set_pos();
	set_grp_number();
	// Start print stats processes.
	process_start(&print_stats_process, NULL);
	// Start Glossy busy-waiting process.
	process_start(&glossy_process, NULL);
	process_start(&app_process, NULL);
	process_start(&app_alt_process, NULL);
	// Start Glossy experiment in one second.
	rtimer_set(&rt, RTIMER_NOW() + RTIMER_SECOND, 1, (rtimer_callback_t)glossy_scheduler, NULL);
	controller = CNTRL_GLOSSY;
	PROCESS_END();
}

/** @} */
/** @} */
/** @} */
