/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \file
 *         App core, header file.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#ifndef APP_ALT_H_
#define APP_ALT_H_

#include "node-id.h"
#include "contiki.h"
#include "dev/watchdog.h"
#include "dev/cc2420_const.h"
#include "cc2420.h"
#include "dev/leds.h"
#include "dev/spi.h"
#include <stdio.h>
#include <legacymsp430.h>
#include <stdlib.h>

/**
 * If not zero, nodes print additional debug information (disabled by default).
 */
#define APP_ALT_DEBUG 0

#define RECOVERY 0
#define RESTRICTION 0
#define QUALITY -60
#define IGNORE 1
#define minicastRV 1
#define USER_DATA_LEN 1

#define MAX_REL 40
#define HOPS 4
// static uint8_t data_map[MAX_REL] = {27, 32, 16, 27, 17, 18, 3, 37, 7, 12, 33, 12, 30, 35, 36, 23, 1, 12, 38, 33, 29, 13, 16, 8, 26, 8, 15, 1, 37, 26, 1, 40, 25, 33, 1, 21, 13, 3, 29, 21};
// static uint8_t data_len_map[MAX_REL] = {50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50};
// static uint8_t chain_array[MAX_REL] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
// static uint8_t chain_array[HOPS] = {1,3,5,9,20,32,43,48};
// static uint8_t chain_array[HOPS] = {1,18,20,24,28,31,33,37,41,43,46,48};
// static uint8_t chain_array[HOPS] = {1,7,10,12,16,19,20,21,31,36,48};
// static uint8_t chain_array[HOPS] = {1,3,5,7,9,29};
static uint8_t chain_array[HOPS] = {1,6,21,101};

/**
 * Initiator timeout, in number of Glossy slots.
 * When the timeout expires, if the initiator has not received any packet
 * after its first transmission it transmits again.
 */
#define APP_ALT_INITIATOR_TIMEOUT      3

// #define N_TX_P                     50

/**
 * Ratio between the frequencies of the DCO and the low-frequency clocks
 */

#if COOJA
#define CLOCK_PHI                     (4194304uL / RTIMER_SECOND)
#else
#define CLOCK_PHI                     (F_CPU / RTIMER_SECOND)
#endif /* COOJA */

#define APP_ALT_HEADER                 0xa0
#define APP_ALT_HEADER_MASK            0xf0
#define APP_ALT_HEADER_LEN             sizeof(uint8_t)
#define APP_ALT_RELAY_CNT_LEN          sizeof(uint8_t)
#define APP_ALT_CHAIN_CNT_LEN          sizeof(uint8_t)
#define APP_ALT_IS_ON()                (get_app_alt_state() != APP_ALT_STATE_OFF)
#define APP_ALT_FOOTER_LEN                    2
#define APP_ALT_FOOTER1_CRC_OK                0x80
#define APP_ALT_FOOTER1_CORRELATION           0x7f

#define APP_ALT_LEN_FIELD              app_alt_packet[0]
#define APP_ALT_HEADER_FIELD           app_alt_packet[1]
#define APP_ALT_CHAIN_CNT_FIELD        app_alt_packet[2]
#define APP_ALT_RELAY_CNT_FIELD        app_alt_packet[3]
#define APP_ALT_DATA_FIELD             app_alt_packet[4]
#define APP_ALT_RSSI_FIELD             app_alt_packet[app_alt_packet_len_tmp - 1]
#define APP_ALT_CRC_FIELD              app_alt_packet[app_alt_packet_len_tmp]

enum {
	APP_ALT_INITIATOR = 1, APP_ALT_RECEIVER = 0
};

/**
 * List of possible Glossy states.
 */
enum app_alt_state {
	APP_ALT_STATE_OFF,          /**< Glossy is not executing */
	APP_ALT_STATE_WAITING,      /**< Glossy is waiting for a packet being flooded */
	APP_ALT_STATE_RECEIVING,    /**< Glossy is receiving a packet */
	APP_ALT_STATE_RECEIVED,     /**< Glossy has just finished receiving a packet */
	APP_ALT_STATE_TRANSMITTING, /**< Glossy is transmitting a packet */
	APP_ALT_STATE_TRANSMITTED,  /**< Glossy has just finished transmitting a packet */
	APP_ALT_STATE_ABORTED,      /**< Glossy has just aborted a packet reception */
	APP_ALT_STATE_RECOVERY		/**< Complicated */
};
#if APP_ALT_DEBUG
unsigned int high_T_irq, rx_timeout, bad_length, bad_header, bad_crc;
#endif /* APP_ALT_DEBUG */

void print_app_alt_states(void);

PROCESS_NAME(app_alt_process);

inline void state_machine_app_alt(unsigned short app_alt_tbiv);

/* ----------------------- Application interface -------------------- */
/**
 * \defgroup glossy_interface Glossy API
 * @{
 * \file   glossy.h
 * \file   glossy.c
 */

/**
 * \defgroup glossy_main Interface related to flooding
 * @{
 */

/**
 * \brief            Start Glossy and stall all other app_altlication tasks.
 *
 * \param data_      A pointer to the flooding data.
 *
 *                   At the initiator, Glossy reads from the given memory
 *                   location data provided by the app_altlication.
 *
 *                   At a receiver, Glossy writes to the given memory
 *                   location data for the app_altlication.
 * \param data_len_  Length of the flooding data, in bytes.
 * \param initiator_ Not zero if the node is the initiator,
 *                   zero if it is a receiver.
 * \param sync_      Not zero if Glossy must provide time synchronization,
 *                   zero otherwise.
 * \param tx_max_    Maximum number of transmissions (N).
 * \param header_    Application-specific header (value between 0x0 and 0xf).
 * \param t_stop_    Time instant at which Glossy must stop, in case it is
 *                   still running.
 * \param cb_        Callback function, called when Glossy terminates its
 *                   execution.
 * \param rtimer_    First argument of the callback function.
 * \param ptr_       Second argument of the callback function.
 */
void app_alt_start(uint8_t app_alt_initiator_, uint8_t node_pos_, uint8_t app_alt_num_nodes_,uint8_t group_number_, uint8_t app_alt_tx_max_,
		uint8_t app_alt_chain_len_, uint8_t app_alt_header_, rtimer_clock_t app_alt_t_stop_, rtimer_callback_t app_alt_cb_,
		struct rtimer *app_alt_rtimer_, void *app_alt_ptr_);

/**
 * \brief            Stop Glossy and resume all other app_altlication tasks.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 * \sa               get_rx_cnt
 */
uint8_t app_alt_stop(void);

/**
 * \brief            Get the last received counter.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 */
uint8_t get_app_alt_rx_cnt(void);
uint8_t get_app_alt_tx_cnt(void);
unsigned long get_rel_cnt(void);

/**
 * \brief            Get the current APP_ALT state.
 * \return           Current Glossy state, one of the possible values
 *                   of \link glossy_state \endlink.
 */
uint8_t get_app_alt_state(void);
uint8_t get_app_alt_rx_cnt_self(void);
uint8_t get_app_alt_rx_cnt_other(void);
uint16_t get_app_alt_grp_init(void);
uint8_t get_packet_len(void);
void print_power_array(void);


/* ----------------------- Interrupt functions ---------------------- */
/**
 * \defgroup app_alt_interrupts Interrupt functions
 * @{
 */
uint8_t app_alt_header_curr,app_alt_header_org,app_alt_header_start,app_alt_header_field_start,app_alt_header_field_curr,bad_hdr_cnt;
inline void app_alt_end_rx_or_tx(void);
inline void app_alt_begin_rx(void);
inline void app_alt_end_rx(void);
inline void app_alt_begin_tx(void);
inline void app_alt_end_tx(void);
/** @} */

/**
 * \defgroup glossy_capture Timer capture of clock ticks
 * @{
 */



/**
 * \brief            Get low-frequency time of first packet reception
 *                   during the last Glossy phase.
 * \returns          Low-frequency time of first packet reception
 *                   during the last Glossy phase.
 */
rtimer_clock_t get_app_alt_t_first_rx_l(void);
rtimer_clock_t get_app_alt_t_first_rx_self(void);
rtimer_clock_t get_app_alt_t_start(void);

#endif /* APP_ALT_H_ */

/** @} */
