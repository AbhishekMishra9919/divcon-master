/*
 * Copyright (c) 2007, Swedish Institute of Computer Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 * @(#)$Id: cc2420.c,v 1.51 2010/04/08 18:23:24 adamdunkels Exp $
 */
/*
 * This code is almost device independent and should be easy to port.
 */

#include <stdio.h>
#include <string.h>

#include "contiki.h"

#include "dev/leds.h"
#include "dev/spi.h"
#include "dev/cc2420.h"
#include "dev/cc2420_const.h"
#include "glossy.h"

/*---------------------------------------------------------------------------*/
#define AUTOACK (1 << 4)
#define ADR_DECODE (1 << 11)
#define RXFIFO_PROTECTION (1 << 9)
#define CORR_THR(n) (((n) & 0x1f) << 6)
#define FIFOP_THR(n) ((n) & 0x7f)
#define RXBPF_LOCUR (1 << 13);

//static inline void radio_abort_rx(void) {
//	state_glossy = GLOSSY_STATE_ABORTED;
//	state_radio = STATE_RADIO_WAITING;
//	radio_flush_rx();
//}

//static inline void tdma_abort_rx(void) {
//
//	//state_tdma = STATE_TDMA_RECEPTION_ABORTED;
//	state_radio = STATE_RADIO_WAITING;
//	radio_flush_rx();
//}



//static inline void radio_start_tx(void) {
//	FASTSPI_STROBE(CC2420_STXON);
//#if ENERGEST_CONF_ON
////	ENERGEST_OFF(ENERGEST_TYPE_LISTEN);
////	ENERGEST_ON(ENERGEST_TYPE_TRANSMIT);
//	if (energest_current_mode[ENERGEST_TYPE_LISTEN]){
//		ENERGEST_OFF(ENERGEST_TYPE_LISTEN);
//		ENERGEST_ON(ENERGEST_TYPE_TRANSMIT);
//	}
//
//#endif /* ENERGEST_CONF_ON */
//}

//static inline void radio_write_tx(void) {
//	FASTSPI_WRITE_FIFO(packet, packet_len_tmp - 1);
//}
//static inline void radio_write_tx_other(void) {
//	FASTSPI_WRITE_FIFO(recev_packet, packet_len_tmp - 1);
//}



/* --------------------------- Radio functions ---------------------- */

// Setting the transmission power to pow
inline void fast_set_power(uint8_t pow){
	SPI_ENABLE();
	// 0xa0ff is the initial value of the CC2420_TXCTRL register measured by me
	SPI_SET_TXPOWER((0xa0ff & 0xffe0) | (pow & 0x1f));
	SPI_DISABLE();
}


inline void radio_flush_tx(void) {
	FASTSPI_STROBE(CC2420_SFLUSHTX);
}

inline uint8_t radio_status(void) {
	uint8_t status;
	FASTSPI_UPD_STATUS(status);
	return status;
}

inline void radio_on(void) {
	FASTSPI_STROBE(CC2420_SRXON);
	while(!(radio_status() & (BV(CC2420_XOSC16M_STABLE))));
	ENERGEST_ON(ENERGEST_TYPE_LISTEN);
}

inline void radio_off(void) {
#if ENERGEST_CONF_ON
	if (energest_current_mode[ENERGEST_TYPE_TRANSMIT]) {
		ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
	}
	if (energest_current_mode[ENERGEST_TYPE_LISTEN]) {
		ENERGEST_OFF(ENERGEST_TYPE_LISTEN);
	}
#endif /* ENERGEST_CONF_ON */
	FASTSPI_STROBE(CC2420_SRFOFF);
}

inline void radio_flush_rx(void) {
	uint8_t dummy;
	FASTSPI_READ_FIFO_BYTE(dummy);
	FASTSPI_STROBE(CC2420_SFLUSHRX);
	FASTSPI_STROBE(CC2420_SFLUSHRX);
}

inline void radio_abort_rx() {
	radio_flush_rx();
}

inline void radio_abort_tx(void) {
	FASTSPI_STROBE(CC2420_SRXON);
#if ENERGEST_CONF_ON
	if (energest_current_mode[ENERGEST_TYPE_TRANSMIT]) {
		ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
		ENERGEST_ON(ENERGEST_TYPE_LISTEN);
	}
#endif /* ENERGEST_CONF_ON */
	radio_flush_rx();
}

inline void radio_start_tx(void) {
	FASTSPI_STROBE(CC2420_STXON);
#if ENERGEST_CONF_ON
	ENERGEST_OFF(ENERGEST_TYPE_LISTEN);
	ENERGEST_ON(ENERGEST_TYPE_TRANSMIT);
#endif /* ENERGEST_CONF_ON */
}

inline void radio_write_tx(uint8_t *packet_ptr, uint8_t packet_len_tmp) {
	// fast_set_power(31);
	FASTSPI_WRITE_FIFO(packet_ptr, packet_len_tmp-1);
	// fast_set_power(0);
	// FASTSPI_WRITE_FIFO(packet_ptr+7, 7);
	// fast_set_power(31);
	// FASTSPI_WRITE_FIFO(packet_ptr+14, 2);
}

/* ------------------------------ Timeouts -------------------------- */
inline void glossy_schedule_rx_timeout(rtimer_clock_t t_rx_timeout) {
	TBCCR5 = t_rx_timeout;
	TBCCTL5 = CCIE;
}

inline void glossy_stop_rx_timeout(void) {
	TBCCTL5 = 0;
}

/*---------------------------------------------------------------------------*/
int
cc2420_init(void)
{
  uint16_t reg;
  {
    int s = splhigh();
    spi_init();

    /* all input by default, set these as output */
    P4DIR |= BV(CSN) | BV(VREG_EN) | BV(RESET_N);

    SPI_DISABLE();                /* Unselect radio. */
    DISABLE_FIFOP_INT();
    FIFOP_INT_INIT();
    splx(s);
  }

  /* Turn on voltage regulator and reset. */
  SET_VREG_ACTIVE();
  SET_RESET_ACTIVE();
  clock_delay(127);
  SET_RESET_INACTIVE();

  /* Turn on the crystal oscillator. */
  FASTSPI_STROBE(CC2420_SXOSCON);

  /* Turn off automatic packet acknowledgment and address decoding. */
  FASTSPI_GETREG(CC2420_MDMCTRL0, reg);
  reg &= ~(AUTOACK | ADR_DECODE);
  FASTSPI_SETREG(CC2420_MDMCTRL0, reg);

  /* Change default values as recomended in the data sheet, */
  /* correlation threshold = 20, RX bandpass filter = 1.3uA. */
  FASTSPI_SETREG(CC2420_MDMCTRL1, CORR_THR(20));
  FASTSPI_GETREG(CC2420_RXCTRL1, reg);
  reg |= RXBPF_LOCUR;
  FASTSPI_SETREG(CC2420_RXCTRL1, reg);

  /* Set the FIFOP threshold to maximum. */
  FASTSPI_SETREG(CC2420_IOCFG0, FIFOP_THR(127));

  /* Turn off "Security enable" (page 32). */
  FASTSPI_GETREG(CC2420_SECCTRL0, reg);
  reg &= ~RXFIFO_PROTECTION;
  FASTSPI_SETREG(CC2420_SECCTRL0, reg);

  radio_flush_rx();

  return 1;
}
/*---------------------------------------------------------------------------*/
void
cc2420_set_channel(int c)
{
  uint16_t f;

  /*
   * Subtract the base channel (11), multiply by 5, which is the
   * channel spacing. 357 is 2405-2048 and 0x4000 is LOCK_THR = 1.
   */

  f = 5 * (c - 11) + 357 + 0x4000;
  /*
   * Writing RAM requires crystal oscillator to be stable.
   */
  while(!(radio_status() & (BV(CC2420_XOSC16M_STABLE))));

  /* Wait for any transmission to end. */
//  while(radio_status() & BV(CC2420_TX_ACTIVE));

  FASTSPI_SETREG(CC2420_FSCTRL, f);
}

/*-------------------------------------
 * Preamble related functions
 */

inline void set_minimum_preamble_length(){

	uint16_t reg;
	FASTSPI_GETREG(CC2420_MDMCTRL0, reg);
	//Last 4 bits 0:3 defines the number of leading zero bytes

	//0 for 1 leading zero byte; so preamble length is 1 zero byte + 2 syncword (SFD) = 3 bytes
	uint16_t preamble_length = 0xFFF0;
	//Not IEEE 802.15.4 compliant

	 //1 for 2 leading zero byte; so preamble length is 2 zero byte + 2 syncword (SFD) = 4 bytes
	 //uint16_t preamble_length = 0xFFF1;
	 //Not IEEE 802.15.4 compliant

	 //2 for 3 leading zero byte; so preamble length is 3 zero byte + 2 syncword (SFD) = 5 bytes
	 //uint16_t preamble_length = 0xFFF2;
	 //IEEE 802.15.4 compliant

	 //There can be max 16 leading zero sysmbols.
	 //uint16_t preamble_length = 0xFFFF

	reg &= preamble_length;
	FASTSPI_SETREG(CC2420_MDMCTRL0, reg);

}

inline void set_default_preamble_length(){

	uint16_t reg;
	FASTSPI_GETREG(CC2420_MDMCTRL0, reg);
	uint16_t preamble_length = 0xFFF2;
	//IEEE 802.15.4 compliant
	reg &= preamble_length;
	FASTSPI_SETREG(CC2420_MDMCTRL0, reg);

}
void
cc2420_set_frequency(int c)
{
	uint16_t f;

	f = c - 2405 + 357 + 0x4000;

	  /*
	   * Writing RAM requires crystal oscillator to be stable.
	   */
	  while(!(radio_status() & (BV(CC2420_XOSC16M_STABLE))));

	  /* Wait for any transmission to end. */
	  while(radio_status() & BV(CC2420_TX_ACTIVE));

	  FASTSPI_SETREG(CC2420_FSCTRL, f);
}

