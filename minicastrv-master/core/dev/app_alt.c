/*
* Copyright (c)
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. Neither the name of the Institute nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
* OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
* SUCH DAMAGE.
*
* Author: Sudipta Saha <sudipta@iitbbs.ac.in>
*
*/

/**
* \file
*         App core, source file.
* \author
*         Sudipta Saha <sudipta@iitbbs.ac.in>
*/

#include "app.h"
#include "app_alt.h"
#include "glossy.h"

static uint8_t app_alt_node_pos, app_alt_initiator, app_alt_rx_cnt, app_alt_tx_cnt, app_alt_tx_max, app_alt_rx_cnt_main;
static uint8_t *app_alt_packet;
static uint8_t app_alt_data_len, app_alt_packet_len, app_alt_packet_len_tmp, app_alt_header, group_number;
static uint8_t app_alt_bytes_read, app_alt_tx_relay_cnt_last, app_alt_rx_relay_cnt_last, app_alt_n_timeouts;
static volatile uint8_t app_alt_state;
// static rtimer_clock_t app_alt_t_rx_start, app_alt_t_rx_stop, app_alt_t_rx_stop_tmp, app_alt_t_tx_start, app_alt_t_tx_stop;
// static rtimer_clock_t app_alt_t_rx_timeout, app_alt_rx_timeout;
// static rtimer_clock_t app_alt_T_irq;
static rtimer_clock_t app_alt_t_stop, app_alt_t_start;
static rtimer_callback_t app_alt_cb;
static struct rtimer *app_alt_rtimer;
static void *app_alt_ptr;
//static unsigned short ie1, ie2, p1ie, p2ie, tbiv;
static unsigned short app_alt_tbiv;

/*********** Analysis related variables ********/

#define MAX_NEW 100
#define MAX_NEW_BIG 200

static int diff, rssi, last_tx_relay_cnt, error, total, rssi_backup;
static uint8_t app_alt_chain_cnt,app_alt_chain_len,app_alt_pos,app_alt_num_nodes,app_alt_num_tx,app_alt_n_tx;

static uint8_t power_array[MAX_NEW_BIG];
static uint8_t app_alt_data_storage[MAX_NEW_BIG];

static uint8_t app_alt_relay_storage[MAX_NEW_BIG];
static uint8_t app_alt_data_bitmap[MAX_NEW_BIG];
// static uint8_t app_alt_state_storage[MAX_NEW];
static uint8_t app_alt_chain_storage[MAX_NEW_BIG];
// static int app_alt_rssi_storage[MAX_NEW];
static rtimer_clock_t app_alt_time_storage[MAX_NEW_BIG];
static rtimer_clock_t app_alt_start_time, prev_backup;

static unsigned int app_alt_current_itr=0;
static unsigned int data_counter = 0;
static uint8_t app_alt_state_backup,app_alt_rssi_field_backup,app_alt_data_field_backup,app_alt_len_field_backup;
static uint8_t app_alt_chain_cnt_field_backup,app_alt_relay_cnt_field_backup,app_alt_chain_len_backup,app_alt_data;

// static uint8_t chain_array[HOPS] = {1,9,17,29,36};

/*********** Analysis related variables ********/


inline void state_machine_app_alt(unsigned short app_alt_tbiv_){
 // read TBIV to clear IFG
 //app_alt_tbiv = TBIV;

 app_alt_tbiv = app_alt_tbiv_;

 if (app_alt_state == APP_ALT_STATE_WAITING && SFD_IS_1) {
   //			// packet reception has started
   app_alt_begin_rx();
 } else {
   if (app_alt_state == APP_ALT_STATE_RECEIVED && SFD_IS_1) {
     //				// packet transmission has started
     app_alt_begin_tx();
   } else {
     if (app_alt_state == APP_ALT_STATE_TRANSMITTING && !SFD_IS_1) {
       // packet transmission has finished
       app_alt_end_tx();
     } else {
       if(app_alt_state == APP_ALT_STATE_RECEIVING && !SFD_IS_1){
         app_alt_end_rx();
       }else {
         if (app_alt_state == APP_ALT_STATE_ABORTED) {
           // packet reception has been aborted
           app_alt_state = APP_ALT_STATE_WAITING;
         } else {
           app_alt_state = APP_ALT_STATE_WAITING;
 // 					if ((app_alt_state == APP_ALT_STATE_WAITING) && (app_alt_tbiv == TBIV_TBCCR4)) {

 // 						// initiator timeout
 // 						app_alt_n_timeouts++;
 // 						if (app_alt_rx_cnt == 0) {
 // 							// no packets received so far: send the packet again
 // 							app_alt_tx_cnt = 0;
 // 							// set the packet length field to the app_altropriate value
 // 							APP_ALT_LEN_FIELD = app_alt_packet_len_tmp;
 // 							//								// set the header field
 // 							APP_ALT_HEADER_FIELD = APP_ALT_HEADER | (app_alt_header & ~APP_ALT_HEADER_MASK);
 // 							if (app_alt_sync) {
 // 								APP_ALT_RELAY_CNT_FIELD = app_alt_n_timeouts * APP_ALT_INITIATOR_TIMEOUT;
 // 							}
 // 							// copy the app_altlication data to the data field
 // 							memcpy(&APP_ALT_DATA_FIELD, app_alt_data, app_alt_data_len);
 // 							//								// set Glossy state
 // 							app_alt_state = APP_ALT_STATE_RECEIVED;
 // 							state_radio = STATE_RADIO_TRANSMITTING;
 // 							//								// write the packet to the TXFIFO
 // 							radio_write_tx(app_alt_packet, app_alt_packet_len_tmp);
 // 							// start another transmission
 // 							radio_start_tx();
 // 							//								// schedule the timeout again
 // 							app_alt_schedule_initiator_timeout();
 // 						} else {
 // 							//								// at least one packet has been received: just stop the timeout
 // 							app_alt_stop_initiator_timeout();
 // 						}
 // 					} else {
 // 						if (app_alt_tbiv == TBIV_TBCCR5) {
 // 							//								// rx timeout
 // 							if (app_alt_state == APP_ALT_STATE_RECEIVING) {
 // 								// we are still trying to receive a packet: abort the reception
 // 								radio_abort_rx();
 // 								app_alt_state = APP_ALT_STATE_WAITING;
 // 								state_radio = STATE_RADIO_WAITING;

 // #if APP_ALT_DEBUG
 // 								app_alt_rx_timeout++;
 // #endif /* GLOSSY_DEBUG */
 // 							}
 // 							//								// stop the timeout
 // 							glossy_stop_rx_timeout();
 // 						} else {
 // 							if (app_alt_state != APP_ALT_STATE_OFF) {
 // 								//									// something strange is going on: go back to the waiting state
 // 								radio_flush_rx();
 // 								app_alt_state = APP_ALT_STATE_WAITING;
 // 								state_radio = STATE_RADIO_WAITING;
 // 							}
 // 						}
 // 					}
         }
       }
     }
   }
 }
}

/* --------------------------- Glossy process ----------------------- */
PROCESS(app_alt_process, "App busy-waiting process");
PROCESS_THREAD(app_alt_process, ev, data) {
 PROCESS_BEGIN();

 do {
   app_alt_packet = (uint8_t *) malloc(128);
 } while (app_alt_packet == NULL);

 while (1) {
   PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL);
   // prevent the Contiki main cycle to enter the LPM mode or
   // any other process to run while Glossy is running
   while (APP_ALT_IS_ON() && RTIMER_CLOCK_LT(RTIMER_NOW(), app_alt_t_stop));

#if COOJA
   // while (get_app_alt_state() != APP_ALT_STATE_OFF);
#endif /* COOJA */
   // Glossy finished: execute the callback function
   dint();
   app_alt_cb(app_alt_rtimer, app_alt_ptr);
   eint();
 }

 PROCESS_END();
}


/* --------------------------- Main interface ----------------------- */
void app_alt_start(uint8_t app_alt_initiator_, uint8_t app_alt_node_pos_, uint8_t app_alt_num_nodes_, uint8_t group_number_, uint8_t app_alt_tx_max_,
     uint8_t app_alt_chain_len_, uint8_t app_alt_header_, rtimer_clock_t app_alt_t_stop_, rtimer_callback_t app_alt_cb_,
     struct rtimer *app_alt_rtimer_, void *app_alt_ptr_) {
 // copy function arguments to the respective Glossy variables


 app_alt_node_pos = app_alt_node_pos_;
 app_alt_initiator = app_alt_initiator_;
 app_alt_chain_len = app_alt_chain_len_;
 app_alt_chain_len_backup = app_alt_chain_len;
 app_alt_header = app_alt_header_;

 group_number = group_number_;

 app_alt_t_stop = app_alt_t_stop_;
 app_alt_cb = app_alt_cb_;
 app_alt_rtimer = app_alt_rtimer_;
 app_alt_ptr = app_alt_ptr_;

 // app_alt_sync = 1;
 app_alt_num_nodes = app_alt_num_nodes_;
 app_alt_n_tx = app_alt_tx_max_;
 app_alt_num_tx = 1;

 // if(app_alt_num_nodes % (app_alt_chain_len-2) == 0) app_alt_num_tx = app_alt_num_nodes/(app_alt_chain_len-2);
 // else app_alt_num_tx = (app_alt_num_nodes/(app_alt_chain_len-2)) + 1;

 app_alt_tx_max = app_alt_n_tx * app_alt_num_tx;
 last_tx_relay_cnt = -1;
 app_alt_rx_relay_cnt_last = 0;
 app_alt_rx_cnt_main = 0;
 app_alt_data_len = USER_DATA_LEN;
 app_alt_data = 1;

 uint8_t i;
 for(i=0;i<MAX_NEW_BIG;i++){
   app_alt_chain_storage[i]=0;
   app_alt_relay_storage[i]=0;
   app_alt_time_storage[i]=0;
   // app_alt_state_storage[i]=0;
   // app_alt_rssi_storage[i]=0;
   /* added for the next file.*/
   app_alt_data_global_storage[i] = 0; // make sure MAX_NEW_BIG <= this array size.
   app_alt_data_bitmap[i]=0;
 }

 for(i=0;i<app_alt_chain_len;i++){
   app_alt_data_storage[i] = 0;
   power_array[i] = 0;
 }
 power_array[0]=1;
 power_array[app_alt_chain_len-1]=1;

 // power_array[0] = 1;
 // for(i=app_alt_chain_len;i<MAX_NEW;i+=app_alt_chain_len){
 // 	power_array[i-1] = 1;
 // 	power_array[i] = 1;
 // }

 if(app_alt_node_pos > 0 && app_alt_node_pos <= app_alt_num_nodes){
   // app_alt_pos = ((app_alt_node_pos-1)/(app_alt_chain_len-2))*app_alt_chain_len + ((app_alt_node_pos-1)%(app_alt_chain_len-2)) + 1;
   app_alt_pos = app_alt_node_pos;
   power_array[app_alt_pos] = 1;
   app_alt_data_storage[app_alt_pos] = cumml_;
   app_alt_rx_cnt_main++;
 }

 // if(app_alt_node_pos>0 && app_alt_node_pos<app_alt_chain_len-1){
 // 	app_alt_data_storage[app_alt_node_pos] = node_id;
 // 	power_array[app_alt_node_pos] = 1;
 // 	app_alt_rx_cnt_main++;
 // }

 // uint8_t index=0;
 // if(index=0;index<app_alt_chain_len;index++){
 // 	app_alt_data_storage[index] = 0;
 // 	power_array[index] = 1;
 // 	app_alt_rx_cnt_main++;
 // }

 // uint8_t index=0;
 // for(index=0;index<app_alt_chain_len;index++){
 // 	power_array[index] = power_array_[index];
 // 	if(power_array[index]) app_alt_data_storage[index] = node_id;
 // }

 // disable all interrupts that may interfere with Glossy
 glossy_disable_other_interrupts();
 //	// initialize Glossy variables
 app_alt_tx_cnt = 0;
 app_alt_rx_cnt = 0;
 error = 0;
 total = 0;
 // app_alt_rx_cnt_other = 0;
 app_alt_current_itr = 0;
 app_alt_chain_cnt = 0;
 data_counter = 0;
 // app_alt_t_start = RTIMER_NOW_DCO();
 app_alt_start_time = RTIMER_NOW();

 APP_ALT_RELAY_CNT_FIELD = 0;
 APP_ALT_CHAIN_CNT_FIELD = 0;

 // set Glossy packet length, with or without relay counter depending on the sync flag value
 // if (app_alt_data_len) {
 app_alt_packet_len_tmp = app_alt_data_len + APP_ALT_FOOTER_LEN + APP_ALT_CHAIN_CNT_LEN + APP_ALT_RELAY_CNT_LEN + APP_ALT_HEADER_LEN;
 app_alt_packet_len = app_alt_packet_len_tmp;

 set_chain_len();
 set_packet_size();
 set_data();

   //		// set the packet length field to the app_altropriate value
 APP_ALT_LEN_FIELD = app_alt_packet_len_tmp;
 // set the header field
 APP_ALT_HEADER_FIELD = APP_ALT_HEADER | (app_alt_header & ~APP_ALT_HEADER_MASK);
 /*--------------------------------------The below values are printed correctly--------------------------------------------*/
 app_alt_header_start=app_alt_header;
 app_alt_header_field_start=APP_ALT_HEADER_FIELD;
 // } else {
 // 	// packet length not known yet (only for receivers)
 // 	app_alt_packet_len = 0;
 // }
 if (app_alt_initiator) {
   // initiator: copy the app_altlication data to the data field
   // memcpy(&APP_ALT_DATA_FIELD, app_alt_data, app_alt_data_len);
   // set Glossy state
   app_alt_state = APP_ALT_STATE_RECEIVED;
   state_radio = STATE_RADIO_TRANSMITTING;

 } else {
   // receiver: set Glossy state
   app_alt_state = APP_ALT_STATE_WAITING;
   state_radio = STATE_RADIO_WAITING;

 }
 // if (app_alt_sync) {
   // set the relay_cnt field to 0
   // APP_ALT_RELAY_CNT_FIELD = 0;
   // APP_ALT_CHAIN_CNT_FIELD = 0;
   // the reference time has not been updated yet
   //t_ref_l_updated = 0;
 // }

 fast_set_power(MAX_POWER_LEVEL);

#if !COOJA
 // resynchronize the DCO
 msp430_sync_dco();
#endif /* COOJA */

 // flush radio buffers
 radio_flush_rx();
 radio_flush_tx();

 if (app_alt_initiator) {
   // write the packet to the TXFIFO

   make_packet();

   // t_start = RTIMER_NOW();
   radio_write_tx(app_alt_packet, app_alt_packet_len_tmp);
   // start the first transmission
   radio_start_tx();
   // schedule the initiator timeout
   //if ((!sync) || app_alt_T_slot_h) {
   // app_alt_n_timeouts = 0;
   // app_alt_schedule_initiator_timeout();
   //}
 } else {
   // turn on the radio
   // t_start = RTIMER_NOW();
   radio_on();
 }
 // activate the Glossy busy waiting process
 process_poll(&app_alt_process);
}

uint8_t app_alt_stop(void) {
 // stop the initiator timeout, in case it is still active
 // app_alt_stop_initiator_timeout();
 // turn off the radio
 radio_off();
 //
 //	// flush radio buffers
 radio_flush_rx();
 radio_flush_tx();
 //
 app_alt_state = APP_ALT_STATE_OFF;
 state_radio = STATE_RADIO_WAITING;
 //	// re-enable non Glossy-related interrupts
 glossy_enable_other_interrupts();
 // return the number of times the packet has been received
 return app_alt_rx_cnt;
}

void print_app_alt_states(void){
 uint8_t i,j=0;
 // uint8_t prev_relay = 0;
 printf("%2d %2d %2d | ",app_alt_packet_len_tmp,error,total);
 for(i=0;i<app_alt_current_itr;i++){

   // if(app_alt_state_storage[i-1]>=3 && (app_alt_state_storage[i]==2 || i==app_alt_current_itr-1))
   // 	printf(" [%d] ", app_alt_data_bitmap[i]);

   // if(app_alt_state_storage[i]!=2)
   // if(i==0 || app_alt_state_storage[i]>3)
   // printf("%d(%d:%d[%d,%d]) ",app_alt_state_storage[i],app_alt_chain_storage[i],app_alt_relay_storage[i],app_alt_rssi_storage[i],app_alt_data_bitmap[i]);

   // if((i<app_alt_current_itr-1 && app_alt_state_storage[i]==2 && app_alt_state_storage[i+1]!=2) || i==(app_alt_current_itr-1)){
   // 	// if(app_alt_relay_storage[i]>prev_relay){
   // 		// prev_relay = app_alt_relay_storage[i];
   // 		if(app_alt_relay_storage[i]!=1)
   // 		printf("%d:%d ", app_alt_relay_storage[i]-1,app_alt_data_bitmap[i]);
   // 	// }
   // }

   // unsigned long time_measure = (unsigned long)app_alt_time_storage[i] * 1e6 / RTIMER_SECOND;
   // printf("%d:%d(%lu.%lu) ", app_alt_relay_storage[i],app_alt_data_bitmap[i], time_measure/1000, time_measure%1000);

   // printf("%d(%d,%d,%d) ",app_alt_relay_storage[i],app_alt_rssi_storage[i],app_alt_state_storage[i],app_alt_data_bitmap[i]);

   unsigned long time_measure = (unsigned long)app_alt_time_storage[i] * 1e6 / RTIMER_SECOND;
   printf("%d:%d(%lu.%lu)", app_alt_relay_storage[i],app_alt_data_bitmap[i], time_measure/1000, time_measure%1000);
   // printf("%d:%d(%d,%lu.%lu)", app_alt_relay_storage[i],app_alt_data_bitmap[i], app_alt_rssi_storage[i], time_measure/1000, time_measure%1000);

   if(app_alt_chain_storage[j]!=0){
     printf("[%d",app_alt_chain_storage[j]);
     j++;
     while(app_alt_chain_storage[j]!=0 && j<data_counter){
       printf(",%d",app_alt_chain_storage[j]);
       j++;
     }
     printf("] ");
   }
   j++;
   printf(" ");

 }
}

inline void fill_data(uint8_t data, uint8_t len){
 uint8_t i;

 for(i=0;i<len;i++)
   app_alt_packet[i+4] = data;
}

inline void set_packet_size(){
 // app_alt_data_len = data_len_map[APP_ALT_RELAY_CNT_FIELD];
 // app_alt_packet_len_tmp = app_alt_data_len + APP_ALT_FOOTER_LEN + APP_ALT_CHAIN_CNT_LEN + APP_ALT_RELAY_CNT_LEN + APP_ALT_HEADER_LEN;
 // app_alt_packet_len = app_alt_packet_len_tmp;
}

inline void set_data(){
 // app_alt_data = data_map[APP_ALT_RELAY_CNT_FIELD];
}


/* Main logic */

inline void make_packet(){
 // app_alt_pos = ((APP_ALT_RELAY_CNT_FIELD+1) / (app_alt_n_tx*2))*app_alt_chain_len + APP_ALT_CHAIN_CNT_FIELD;
 app_alt_pos = APP_ALT_CHAIN_CNT_FIELD;
 // set_packet_size();

 // uint8_t curr_data = data_map[APP_ALT_RELAY_CNT_FIELD];
 // uint8_t curr_data_len = data_len_map[APP_ALT_RELAY_CNT_FIELD];

 if(power_array[app_alt_pos] || app_alt_pos == app_alt_chain_len-1) {
 // if(power_array[app_alt_pos]) {
   fast_set_power(MAX_POWER_LEVEL);
   fill_data(app_alt_data_storage[app_alt_pos], app_alt_data_len);
   // APP_ALT_DATA_FIELD = app_alt_data_storage[app_alt_chain_cnt];
 }else {
   fast_set_power(MIN_POWER_LEVEL);
   fill_data(0, app_alt_data_len);
   // APP_ALT_DATA_FIELD = 0;
 }

 // APP_ALT_LEN_FIELD = app_alt_packet_len_tmp;

 // fast_set_power(MAX_POWER_LEVEL);
 // fill_data(0, app_alt_data_len);
}

inline void set_chain_len(){
#if minicastRV
 if (APP_ALT_RELAY_CNT_FIELD < HOPS)
   app_alt_chain_len = chain_array[APP_ALT_RELAY_CNT_FIELD]+2;
 else app_alt_chain_len = app_alt_chain_len_backup;
#endif
}

inline void rx_packet_data(){

 // app_alt_state_storage[app_alt_current_itr] = 3;
 // app_alt_chain_storage[app_alt_current_itr] = app_alt_chain_cnt_field_backup+1;
 // app_alt_rssi_storage[app_alt_current_itr] = (int)((signed char)app_alt_rssi_field_backup)-45;
 // app_alt_rssi_storage[app_alt_current_itr] = app_alt_data_field_backup;

 // app_alt_pos = (app_alt_relay_cnt_field_backup / (app_alt_n_tx*2))*app_alt_chain_len + app_alt_chain_cnt_field_backup;

 if(app_alt_rx_cnt==0){
   app_alt_rx_relay_cnt_last = app_alt_relay_cnt_field_backup;
   if(app_alt_tx_cnt==0)
     last_tx_relay_cnt = app_alt_relay_cnt_field_backup-1;
 }

 app_alt_pos = app_alt_chain_cnt_field_backup;
 if(app_alt_data_storage[app_alt_pos] == 0 && app_alt_data_field_backup != 0){
   app_alt_data_storage[app_alt_pos] = app_alt_data_field_backup;
   // we need to map the app_alt_pos somehow, should not depend on the data.
   app_alt_chain_storage[data_counter++] = app_alt_data_field_backup;
   app_alt_rx_cnt_main++;
   power_array[app_alt_pos]=1;
 }

 if (app_alt_relay_cnt_field_backup > app_alt_rx_relay_cnt_last){
   app_alt_relay_storage[app_alt_current_itr] = app_alt_rx_relay_cnt_last + 1;
   app_alt_time_storage[app_alt_current_itr] = prev_backup - app_alt_start_time;
   // app_alt_rssi_storage[app_alt_current_itr] = rssi_backup;
   app_alt_data_bitmap[app_alt_current_itr++] = app_alt_rx_cnt_main;
   app_alt_rx_relay_cnt_last = app_alt_relay_cnt_field_backup;
   app_alt_chain_storage[data_counter++] = 0;
 }

 prev_backup = RTIMER_NOW();
 rssi_backup = (int)((signed char)app_alt_rssi_field_backup)-45;

 app_alt_rx_cnt++;

 // if (app_alt_relay_cnt_field_backup > app_alt_rx_relay_cnt_last){

 // 	compress();
 // 	app_alt_relay_storage[app_alt_current_itr] = app_alt_rx_relay_cnt_last+1;
 // 	app_alt_rx_relay_cnt_last = app_alt_relay_cnt_field_backup;

 // 	// if(app_alt_state_backup != APP_ALT_STATE_RECOVERY)
 // 	app_alt_current_itr++;
 // 	// else app_alt_rx_cnt--;
 // 	// app_alt_rssi_storage[app_alt_chain_itr++]=100;
 // }

 // app_alt_chain_storage[app_alt_chain_cnt_field_backup] = 1;
 // app_alt_rssi_storage[app_alt_chain_itr++] = app_alt_chain_cnt_field_backup+1;
}

// inline void compress(){
// 	uint8_t i,fail=0;

// 	for(i=0;i<app_alt_chain_len;i++)
// 		if(app_alt_chain_storage[i]==0)
// 			fail++;

// 	if(app_alt_chain_storage[app_alt_chain_len-1]==0)
// 		app_alt_data_bitmap[app_alt_current_itr]=1;

// 	if(app_alt_chain_storage[0]==0)
// 		app_alt_rssi_storage[app_alt_current_itr]=1;

// 	app_alt_state_storage[app_alt_current_itr]=fail;

// 	for(i=0;i<app_alt_chain_len;i++)
// 		app_alt_chain_storage[i]=0;
// }

/* Main logic */

uint8_t get_app_alt_rx_cnt(void) {
 return app_alt_rx_cnt;
}
uint8_t get_app_alt_tx_cnt(void) {
 return app_alt_tx_cnt;
}

unsigned long get_rel_cnt(void) {
 uint8_t i;
 for(i=0;i<app_alt_current_itr;i++)
   if(app_alt_data_bitmap[i]==app_alt_chain_len)
     return app_alt_relay_storage[i-1];
}

// uint8_t get_app_alt_relay_cnt(void) {
// 	return app_alt_relay_cnt;
// }

// rtimer_clock_t get_app_alt_T_slot_h(void) {
// 	return app_alt_T_slot_h;
// }

uint8_t get_app_alt_state(void) {
 return app_alt_state;
}

// rtimer_clock_t get_app_alt_t_first_rx_l(void) {
// 	return app_alt_t_first_rx_l;
// }
// rtimer_clock_t get_app_alt_t_first_rx_self(void) {
// 	return app_alt_t_first_rx_self;
// }
// rtimer_clock_t get_app_alt_t_start(void) {
// 	return t_start;
// }

// rtimer_clock_t get_app_alt_t_ref_l(void) {
// 	return app_alt_t_ref_l;
// }

// void set_app_alt_t_ref_l(rtimer_clock_t t) {
// 	app_alt_t_ref_l = t;
// }
uint8_t get_packet_len(void) {
 return app_alt_packet_len_tmp;
}

// uint8_t * get_app_alt_data(void) {
// 	return temp_data;
// }
uint8_t get_app_alt_rx_cnt_main(void) {
 return app_alt_rx_cnt_main;
}
// uint8_t get_app_alt_rx_cnt_other(void) {
// 	return app_alt_rx_cnt_other;
// }

///* ----------------------- Interrupt functions ---------------------- */

inline void app_alt_end_rx_or_tx(){
 if(app_alt_state == APP_ALT_STATE_TRANSMITTING) app_alt_end_tx();
 else app_alt_end_rx();
}

inline void app_alt_begin_rx(void) {
 // app_alt_t_rx_start = TBCCR1;
 app_alt_state = APP_ALT_STATE_RECEIVING;

 total++;

 // //last reception should be immediately followed by transmission
 // if(app_alt_chain_cnt == app_alt_chain_len-1) state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
 // //any other reception is followed by a reception
 // else state_radio = STATE_RADIO_RECEIVING_NORMAL;

 state_radio = STATE_RADIO_RECEIVING_NORMAL;

#if IGNORE
 if (app_alt_node_pos==0){
   radio_abort_rx();
   app_alt_state = APP_ALT_STATE_WAITING;
   state_radio = STATE_RADIO_WAITING;
   return;
 }
#endif

 // if (app_alt_packet_len) {
 // 	// Rx timeout: packet duration + 200 us
 // 	// (packet duration: 32 us * packet_length, 1 DCO tick ~ 0.23 us)
 // 	app_alt_t_rx_timeout = app_alt_t_rx_start + ((rtimer_clock_t)app_alt_packet_len_tmp * 35 + 200) * 4;
 // }
 //
 // wait until the FIFO pin is 1 (i.e., until the first byte is received)
 while (!FIFO_IS_1) {
// 		if (app_alt_packet_len && !RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), app_alt_t_rx_timeout)) {
// 			radio_abort_rx();
// 			app_alt_state = APP_ALT_STATE_WAITING;
// 			state_radio = STATE_RADIO_WAITING;

// 			app_alt_state_storage[app_alt_current_itr++] = 8;

// #if APP_ALT_DEBUG
// 			app_alt_rx_timeout++;
// #endif /* GLOSSY_DEBUG */
// 			return;
// 		}
 };
 // read the first byte (i.e., the len field) from the RXFIFO
 FASTSPI_READ_FIFO_BYTE(APP_ALT_LEN_FIELD);
 // keep receiving only if it has the right length
 if ((APP_ALT_LEN_FIELD < APP_ALT_FOOTER_LEN) || (APP_ALT_LEN_FIELD > 127)) {
   // packet with a wrong length: abort packet reception
   radio_abort_rx();
   app_alt_state = APP_ALT_STATE_WAITING;
   state_radio = STATE_RADIO_WAITING;
   error++;

   // app_alt_state_storage[app_alt_current_itr++] = 4;

#if APP_ALT_DEBUG
   app_alt_bad_length++;
#endif /* GLOSSY_DEBUG */
   return;
 }
 app_alt_bytes_read = 1;
 // if (!app_alt_packet_len) {
 // 	app_alt_packet_len_tmp = APP_ALT_LEN_FIELD;
 // 	app_alt_t_rx_timeout = app_alt_t_rx_start + ((rtimer_clock_t)app_alt_packet_len_tmp * 35 + 200) * 4;
 // }

 app_alt_packet_len_tmp = APP_ALT_LEN_FIELD;
 app_alt_packet_len = app_alt_packet_len_tmp;

#if COOJA

 FASTSPI_READ_FIFO_BYTE(APP_ALT_HEADER_FIELD);
 // keep receiving only if it has the right header
 /*--------------------------------------The below values are printed 0?????--------------------------------------------*/
 app_alt_header_field_curr=APP_ALT_HEADER_FIELD;
 app_alt_header_curr = APP_ALT_HEADER_FIELD & ~APP_ALT_HEADER_MASK;
 app_alt_header_org=app_alt_header;
 if (((APP_ALT_HEADER_FIELD & APP_ALT_HEADER_MASK) != APP_ALT_HEADER) ||((APP_ALT_HEADER_FIELD & ~APP_ALT_HEADER_MASK) != app_alt_header))
   // packet with a wrong header: abort packet reception
   //if ((APP_ALT_HEADER_FIELD & APP_ALT_HEADER_MASK) != APP_ALT_HEADER )
 {
   //bad_hdr_cnt++;
   radio_abort_rx();
   app_alt_state = APP_ALT_STATE_WAITING;
   state_radio = STATE_RADIO_WAITING;
   error++;

   // app_alt_state_storage[app_alt_current_itr++] = 5;

#if APP_ALT_DEBUG
   app_alt_bad_header++;
#endif /* GLOSSY_DEBUG */
   return;
 }
 app_alt_bytes_read = 2;

 FASTSPI_READ_FIFO_BYTE(APP_ALT_CHAIN_CNT_FIELD);

// 	if(APP_ALT_CHAIN_CNT_FIELD > app_alt_chain_len)
// 	{
// 		//bad_hdr_cnt++;
// 		radio_abort_rx();
// 		app_alt_state = APP_ALT_STATE_WAITING;
// 		state_radio = STATE_RADIO_WAITING;
// 		error++;

// 		// app_alt_state_storage[app_alt_current_itr++] = 8;

// #if APP_ALT_DEBUG
// 		app_alt_bad_header++;
// #endif /* GLOSSY_DEBUG */
// 		return;
// 	}

 app_alt_bytes_read = 3;

 FASTSPI_READ_FIFO_BYTE(APP_ALT_RELAY_CNT_FIELD);

 app_alt_bytes_read = 4;

 set_chain_len();
 // set_packet_size();
 // set_data();

 if(APP_ALT_CHAIN_CNT_FIELD == app_alt_chain_len-1)
   state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;

 // this should be there in !COOJA case also don't forget to add there
 // if I should transmit in next tx, (controlled)
 // if (APP_ALT_CHAIN_CNT_FIELD != app_alt_chain_len-1 && APP_ALT_RELAY_CNT_FIELD < HOPS && APP_ALT_CHAIN_CNT_FIELD > chain_array[APP_ALT_RELAY_CNT_FIELD]){
 // 	radio_abort_rx();
 // 	app_alt_state = APP_ALT_STATE_WAITING;
 // 	state_radio = STATE_RADIO_WAITING;
 // 	// state_radio = STATE_RADIO_RECEIVING_NORMAL;
 // 	return;
 // }

#if minicastRV
 if (APP_ALT_RELAY_CNT_FIELD < HOPS-1 && app_alt_node_pos > chain_array[APP_ALT_RELAY_CNT_FIELD+1]){
   // radio_abort_rx();
   // app_alt_state = APP_ALT_STATE_WAITING;
   // state_radio = STATE_RADIO_WAITING;
   state_radio = STATE_RADIO_RECEIVING_NORMAL;
   // return;
 }
#endif

#if RECOVERY

 diff = APP_ALT_RELAY_CNT_FIELD - last_tx_relay_cnt;

 // if(diff == 2 ){
 if(app_alt_rx_cnt > 0 && diff >= 2 && diff%2 == 0){
   //break of and transmit from the next packet
   //switch to recovery
   state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
   app_alt_state = APP_ALT_STATE_RECOVERY;
 }

#endif /* RECOVERY */

 if (app_alt_packet_len_tmp > 8) {
   // if packet is longer than 8 bytes, read all bytes but the last 8
   while (app_alt_bytes_read <= app_alt_packet_len_tmp - 8) {
     // read another byte from the RXFIFO
     FASTSPI_READ_FIFO_BYTE(app_alt_packet[app_alt_bytes_read]);
     app_alt_bytes_read++;
   }
 }

#elif !COOJA

 while (!FIFO_IS_1) {
// 		if (!RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), app_alt_t_rx_timeout)) {

// 			radio_abort_rx();
// 			app_alt_state = APP_ALT_STATE_WAITING;
// 			state_radio = STATE_RADIO_WAITING;

// 			app_alt_state_storage[app_alt_current_itr++] = 8;

// #if APP_ALT_DEBUG
// 			app_alt_rx_timeout++;
// #endif /* GLOSSY_DEBUG */
// 			return;
// 		}
 };
 // read the second byte (i.e., the header field) from the RXFIFO
 FASTSPI_READ_FIFO_BYTE(APP_ALT_HEADER_FIELD);
 // keep receiving only if it has the right header
 /*--------------------------------------The below values are printed 0?????--------------------------------------------*/
 app_alt_header_field_curr=APP_ALT_HEADER_FIELD;
 app_alt_header_curr = APP_ALT_HEADER_FIELD & ~APP_ALT_HEADER_MASK;
 app_alt_header_org=app_alt_header;
 if (((APP_ALT_HEADER_FIELD & APP_ALT_HEADER_MASK) != APP_ALT_HEADER) ||((APP_ALT_HEADER_FIELD & ~APP_ALT_HEADER_MASK) != app_alt_header))
   // packet with a wrong header: abort packet reception
   //if ((APP_ALT_HEADER_FIELD & APP_ALT_HEADER_MASK) != APP_ALT_HEADER )
 {
   //bad_hdr_cnt++;
   radio_abort_rx();
   app_alt_state = APP_ALT_STATE_WAITING;
   state_radio = STATE_RADIO_WAITING;
   error++;

   // app_alt_state_storage[app_alt_current_itr++] = 5;

#if APP_ALT_DEBUG
   app_alt_bad_header++;
#endif /* GLOSSY_DEBUG */
   return;
 }
 app_alt_bytes_read = 2;

 while(!FIFO_IS_1);
 FASTSPI_READ_FIFO_BYTE(APP_ALT_CHAIN_CNT_FIELD);

// 	if(APP_ALT_CHAIN_CNT_FIELD > app_alt_chain_len)
// 	{
// 		//bad_hdr_cnt++;
// 		radio_abort_rx();
// 		app_alt_state = APP_ALT_STATE_WAITING;
// 		state_radio = STATE_RADIO_WAITING;
// 		error++;
// 		// app_alt_state_storage[app_alt_current_itr++] = 5;

// #if APP_ALT_DEBUG
// 		app_alt_bad_header++;
// #endif /* GLOSSY_DEBUG */
// 		return;
// 	}

 app_alt_bytes_read = 3;

 while(!FIFO_IS_1);
 FASTSPI_READ_FIFO_BYTE(APP_ALT_RELAY_CNT_FIELD);

 app_alt_bytes_read = 4;

 set_chain_len();
 // set_packet_size();
 // set_data();

 if(APP_ALT_CHAIN_CNT_FIELD == app_alt_chain_len-1)
   state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;

#if minicastRV
 if (APP_ALT_RELAY_CNT_FIELD < HOPS-1 && app_alt_node_pos > chain_array[APP_ALT_RELAY_CNT_FIELD+1]){
   // radio_abort_rx();
   // app_alt_state = APP_ALT_STATE_WAITING;
   // state_radio = STATE_RADIO_WAITING;
   state_radio = STATE_RADIO_RECEIVING_NORMAL;
   // return;
 }
#endif

#if RECOVERY

 diff = APP_ALT_RELAY_CNT_FIELD - last_tx_relay_cnt;

 // if(diff == 2 ){
 if(app_alt_rx_cnt > 0 && diff >= 2 && diff%2 == 0){
   //break of and transmit from the next packet
   //switch to recovery
   state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
   app_alt_state = APP_ALT_STATE_RECOVERY;
 }

#endif /* RECOVERY */

 if (app_alt_packet_len_tmp > 8) {
   // if packet is longer than 8 bytes, read all bytes but the last 8
   while (app_alt_bytes_read <= app_alt_packet_len_tmp - 8) {
     // wait until the FIFO pin is 1 (until one more byte is received)
     while (!FIFO_IS_1) {
// 				if (!RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), app_alt_t_rx_timeout)) {
// 					radio_abort_rx();
// 					app_alt_state = APP_ALT_STATE_WAITING;
// 					state_radio = STATE_RADIO_WAITING;

// 					app_alt_state_storage[app_alt_current_itr++] = 8;

// #if APP_ALT_DEBUG
// 					app_alt_rx_timeout++;
// #endif /* GLOSSY_DEBUG */
// 					return;
// 				}
     };
     // read another byte from the RXFIFO
     FASTSPI_READ_FIFO_BYTE(app_alt_packet[app_alt_bytes_read]);
     app_alt_bytes_read++;
   }
 }

#endif /* COOJA */

 // glossy_schedule_rx_timeout(app_alt_t_rx_timeout);
}

inline void app_alt_end_rx(void) {
 // rtimer_clock_t app_alt_t_rx_stop_tmp = TBCCR1;
 // read the remaining bytes from the RXFIFO
 FASTSPI_READ_FIFO_NO_WAIT(&app_alt_packet[app_alt_bytes_read], app_alt_packet_len_tmp - app_alt_bytes_read + 1);
 app_alt_bytes_read = app_alt_packet_len_tmp + 1;
#if COOJA
 if ((APP_ALT_CRC_FIELD & APP_ALT_FOOTER1_CRC_OK) && ((APP_ALT_HEADER_FIELD & APP_ALT_HEADER_MASK) == APP_ALT_HEADER)) {
#else
 if (APP_ALT_CRC_FIELD & APP_ALT_FOOTER1_CRC_OK) {
#endif /* COOJA */

   app_alt_state_backup = app_alt_state;
   app_alt_chain_cnt_field_backup = APP_ALT_CHAIN_CNT_FIELD;
   app_alt_data_field_backup = APP_ALT_DATA_FIELD;
   app_alt_rssi_field_backup = APP_ALT_RSSI_FIELD;
   app_alt_relay_cnt_field_backup = APP_ALT_RELAY_CNT_FIELD;
   app_alt_len_field_backup = APP_ALT_LEN_FIELD;

   if(state_radio == STATE_RADIO_RECEIVING_FOR_SYNCH){

     // signed char l_rssi = APP_ALT_RSSI_FIELD;
     // rssi = (int)(l_rssi)-45;

     if (app_alt_tx_cnt == app_alt_tx_max) {
       // no more Tx to perform: stop Glossy
       radio_off();
       app_alt_state = APP_ALT_STATE_OFF;
       state_radio = STATE_RADIO_WAITING;
     }

// #if RESTRICTION
     // else if ((int)((signed char)APP_ALT_RSSI_FIELD)-45 > QUALITY){
// #else
     else{
// #endif /* RESTRICTION */

       if (app_alt_state == APP_ALT_STATE_RECOVERY) {
         // transmit the next packet in the chain
         // if you recieved the last packet, rare case, see later
         app_alt_chain_cnt = APP_ALT_CHAIN_CNT_FIELD;
         app_alt_chain_cnt++;
         APP_ALT_CHAIN_CNT_FIELD = app_alt_chain_cnt;

       }else {
         // write Glossy packet to the TXFIFO
         app_alt_chain_cnt = 0;
         APP_ALT_CHAIN_CNT_FIELD = app_alt_chain_cnt;
         APP_ALT_RELAY_CNT_FIELD++;
         set_chain_len();
         set_packet_size();
         set_data();
       }

       make_packet();

       radio_write_tx(app_alt_packet, app_alt_packet_len_tmp);
       app_alt_state = APP_ALT_STATE_RECEIVED;
       state_radio = STATE_RADIO_TRANSMITTING;
     }

// #if RESTRICTION
// 			else{

// 				app_alt_chain_cnt = 0;
// 				app_alt_state = APP_ALT_STATE_WAITING;
// 				state_radio = STATE_RADIO_WAITING;
// 			}
// #endif /* RESTRICTION */

   }else{

     app_alt_chain_cnt++;

     // ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
     // ENERGEST_ON(ENERGEST_TYPE_LISTEN);

     app_alt_state = APP_ALT_STATE_WAITING;
     state_radio = STATE_RADIO_WAITING;

     // radio_flush_tx();
   }

   /*  ************************ SETTING VARIABLES  ************************ */
   rx_packet_data();

   /*  ************************ SETTING VARIABLES  ************************ */


 } else {

   // app_alt_state_storage[app_alt_current_itr++] = 6;

#if APP_ALT_DEBUG
   app_alt_bad_crc++;
#endif /* GLOSSY_DEBUG */

   // packet corrupted, abort the transmission before it actually starts
   radio_abort_tx();
   app_alt_state = APP_ALT_STATE_WAITING;
   state_radio = STATE_RADIO_WAITING;
   error++;
 }
}
//
inline void app_alt_begin_tx(void) {
 // app_alt_t_tx_start = TBCCR1;
 app_alt_state = APP_ALT_STATE_TRANSMITTING;

 // last transmission needs to be followed by reception
 if(app_alt_chain_cnt == app_alt_chain_len-1) state_radio = STATE_RADIO_TRANSMITTING;
 //every other transmission needs to be immediately followed by another transmission
 else state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;

 last_tx_relay_cnt = APP_ALT_RELAY_CNT_FIELD;

}

inline void app_alt_end_tx(void) {

 //app_alt_current_itr = APP_ALT_RELAY_CNT_FIELD;
 // app_alt_state_storage[app_alt_current_itr] = 2;
 // app_alt_chain_storage[app_alt_current_itr] = APP_ALT_CHAIN_CNT_FIELD+1;
 // app_alt_relay_storage[app_alt_current_itr] = APP_ALT_RELAY_CNT_FIELD+1;
 // app_alt_rssi_storage[app_alt_current_itr] = APP_ALT_DATA_FIELD;
 // app_alt_data_bitmap[app_alt_current_itr++] = app_alt_rx_cnt_main;

 // app_alt_t_tx_stop = TBCCR1;

 if(state_radio == STATE_RADIO_RECEIVING_FOR_SYNCH){
   app_alt_chain_cnt++;
   APP_ALT_CHAIN_CNT_FIELD = app_alt_chain_cnt;

   make_packet();

   // write Glossy packet to the TXFIFO
   radio_write_tx(app_alt_packet, app_alt_packet_len_tmp);
   app_alt_state = APP_ALT_STATE_RECEIVED;
   state_radio = STATE_RADIO_TRANSMITTING;

 }else{
   app_alt_chain_cnt = 0;
   ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
   ENERGEST_ON(ENERGEST_TYPE_LISTEN);
   // stop Glossy if tx_cnt reached tx_max (and tx_max > 1 at the initiator)
   app_alt_tx_cnt++;
   if ((app_alt_tx_cnt == app_alt_tx_max) && !app_alt_initiator) {
     radio_off();
     app_alt_state = APP_ALT_STATE_OFF;
     state_radio = STATE_RADIO_WAITING;
   }else{
     app_alt_state = APP_ALT_STATE_WAITING;
     state_radio = STATE_RADIO_WAITING;
   }
   radio_flush_tx();
 }

}

// inline void app_alt_schedule_initiator_timeout() {
// #if !COOJA
// 	TBCCR4 = app_alt_t_start + (app_alt_n_timeouts + 1) * APP_ALT_INITIATOR_TIMEOUT *
// 			((rtimer_clock_t)app_alt_packet_len * 35 + 400) * 4;

// 	TBCCTL4 = CCIE;
// #endif
// }

// inline void app_alt_stop_initiator_timeout(void) {
// 	TBCCTL4 = 0;
// }

// void print_power_array(void){
// 	uint8_t index;

// 	for(index=0;index<app_alt_num_tx*app_alt_chain_len;index++){
// 		if(index % app_alt_chain_len == 0) printf(" ");
// 		printf("%d ",power_array[index]);
// 	}
// }
